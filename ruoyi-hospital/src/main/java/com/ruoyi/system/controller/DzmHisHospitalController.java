package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DzmHisHospital;
import com.ruoyi.system.service.IDzmHisHospitalService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * HIS医院基本信息Controller
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@RestController
@RequestMapping("/system/hospital")
public class DzmHisHospitalController extends BaseController
{
    @Autowired
    private IDzmHisHospitalService dzmHisHospitalService;

    /**
     * 查询HIS医院基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:list')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisHospital dzmHisHospital)
    {
        startPage();
        List<DzmHisHospital> list = dzmHisHospitalService.selectDzmHisHospitalList(dzmHisHospital);
        return getDataTable(list);
    }

    /**
     * 导出HIS医院基本信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:export')")
    @Log(title = "HIS医院基本信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisHospital dzmHisHospital)
    {
        List<DzmHisHospital> list = dzmHisHospitalService.selectDzmHisHospitalList(dzmHisHospital);
        ExcelUtil<DzmHisHospital> util = new ExcelUtil<DzmHisHospital>(DzmHisHospital.class);
        util.exportExcel(response, list, "HIS医院基本信息数据");
    }

    /**
     * 获取HIS医院基本信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(dzmHisHospitalService.selectDzmHisHospitalById(id));
    }

    /**
     * 新增HIS医院基本信息
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:add')")
    @Log(title = "HIS医院基本信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisHospital dzmHisHospital)
    {
        return toAjax(dzmHisHospitalService.insertDzmHisHospital(dzmHisHospital));
    }

    /**
     * 修改HIS医院基本信息
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:edit')")
    @Log(title = "HIS医院基本信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisHospital dzmHisHospital)
    {
        return toAjax(dzmHisHospitalService.updateDzmHisHospital(dzmHisHospital));
    }

    /**
     * 删除HIS医院基本信息
     */
    @PreAuthorize("@ss.hasPermi('system:hospital:remove')")
    @Log(title = "HIS医院基本信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(dzmHisHospitalService.deleteDzmHisHospitalByIds(ids));
    }
}
