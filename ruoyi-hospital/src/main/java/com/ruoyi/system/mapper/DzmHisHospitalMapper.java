package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DzmHisHospital;

/**
 * HIS医院基本信息Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface DzmHisHospitalMapper 
{
    /**
     * 查询HIS医院基本信息
     * 
     * @param id HIS医院基本信息主键
     * @return HIS医院基本信息
     */
    public DzmHisHospital selectDzmHisHospitalById(String id);

    /**
     * 查询HIS医院基本信息列表
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return HIS医院基本信息集合
     */
    public List<DzmHisHospital> selectDzmHisHospitalList(DzmHisHospital dzmHisHospital);

    /**
     * 新增HIS医院基本信息
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return 结果
     */
    public int insertDzmHisHospital(DzmHisHospital dzmHisHospital);

    /**
     * 修改HIS医院基本信息
     * 
     * @param dzmHisHospital HIS医院基本信息
     * @return 结果
     */
    public int updateDzmHisHospital(DzmHisHospital dzmHisHospital);

    /**
     * 删除HIS医院基本信息
     * 
     * @param id HIS医院基本信息主键
     * @return 结果
     */
    public int deleteDzmHisHospitalById(String id);

    /**
     * 批量删除HIS医院基本信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisHospitalByIds(String[] ids);
}
