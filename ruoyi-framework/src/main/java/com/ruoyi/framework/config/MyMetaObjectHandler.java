package com.ruoyi.framework.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruoyi.edu.util.RandomNumberUtil;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Random;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: MyMetaObjectHandler
 * @package_Name: com.ruoyi.framework.config
 * @User: guohaotian
 * @Date: 2022/6/30 14:18
 * @Description:挂号类填充策略
 * @To change this template use File | Settings | File Templates.
 */

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Autowired
    private RandomNumberUtil randomNumberUtil;

    @Override
    public void insertFill(MetaObject metaObject) {
//        方法第一的参数是自动填充的字段，第二个是要自动填充的值，第三个是指定实体类对象
        this.setFieldValByName("registrationNumber",randomNumberUtil.getRandomNumber(),metaObject);
        this.setFieldValByName("createTime",randomNumberUtil.getCurrentTimeMillis(),metaObject);
        this.setFieldValByName("updateTime",randomNumberUtil.getCurrentTimeMillis(),metaObject);
        this.setFieldValByName("addtime",randomNumberUtil.getCurrentTimeMillis(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateTime",randomNumberUtil.getCurrentTimeMillis(),metaObject);
    }
}
