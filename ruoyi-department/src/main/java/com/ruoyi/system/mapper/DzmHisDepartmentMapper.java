package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DzmHisDepartment;
import org.apache.ibatis.annotations.Mapper;

/**
 * 科室Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-21
 */
@Mapper
public interface DzmHisDepartmentMapper 
{
    /**
     * 查询科室
     * 
     * @param did 科室主键
     * @return 科室
     */
    public DzmHisDepartment selectDzmHisDepartmentByDid(String did);

    /**
     * 查询科室列表
     * 
     * @param dzmHisDepartment 科室
     * @return 科室集合
     */
    public List<DzmHisDepartment> selectDzmHisDepartmentList(DzmHisDepartment dzmHisDepartment);

    /**
     * 新增科室
     * 
     * @param dzmHisDepartment 科室
     * @return 结果
     */
    public int insertDzmHisDepartment(DzmHisDepartment dzmHisDepartment);

    /**
     * 修改科室
     * 
     * @param dzmHisDepartment 科室
     * @return 结果
     */
    public int updateDzmHisDepartment(DzmHisDepartment dzmHisDepartment);

    /**
     * 删除科室
     * 
     * @param did 科室主键
     * @return 结果
     */
    public int deleteDzmHisDepartmentByDid(String did);

    /**
     * 批量删除科室
     * 
     * @param dids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisDepartmentByDids(String[] dids);
}
