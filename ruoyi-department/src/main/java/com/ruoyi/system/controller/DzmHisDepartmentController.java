package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DzmHisDepartment;
import com.ruoyi.system.service.IDzmHisDepartmentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 科室Controller
 *
 * @author ruoyi
 * @date 2022-06-21
 */
@RestController
@RequestMapping("/system/department")
public class DzmHisDepartmentController extends BaseController
{
    @Autowired
    private IDzmHisDepartmentService dzmHisDepartmentService;

    /**
     * 查询科室列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisDepartment dzmHisDepartment)
    {
        startPage();
        List<DzmHisDepartment> list = dzmHisDepartmentService.selectDzmHisDepartmentList(dzmHisDepartment);
        return getDataTable(list);
    }

    /**
     * 导出科室列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "科室", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisDepartment dzmHisDepartment)
    {
        List<DzmHisDepartment> list = dzmHisDepartmentService.selectDzmHisDepartmentList(dzmHisDepartment);
        ExcelUtil<DzmHisDepartment> util = new ExcelUtil<DzmHisDepartment>(DzmHisDepartment.class);
        util.exportExcel(response, list, "科室数据");
    }

    /**
     * 获取科室详细信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping(value = "/{did}")
    public AjaxResult getInfo(@PathVariable("did") String did)
    {
        return AjaxResult.success(dzmHisDepartmentService.selectDzmHisDepartmentByDid(did));
    }

    /**
     * 新增科室
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "科室", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisDepartment dzmHisDepartment)
    {
        return toAjax(dzmHisDepartmentService.insertDzmHisDepartment(dzmHisDepartment));
    }

    /**
     * 修改科室
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "科室", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisDepartment dzmHisDepartment)
    {
        return toAjax(dzmHisDepartmentService.updateDzmHisDepartment(dzmHisDepartment));
    }

    /**
     * 删除科室
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "科室", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dids}")
    public AjaxResult remove(@PathVariable String[] dids)
    {
        return toAjax(dzmHisDepartmentService.deleteDzmHisDepartmentByDids(dids));
    }
}
