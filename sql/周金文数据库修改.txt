DROP TABLE IF EXISTS `dzm_his_doctor`;
CREATE TABLE `dzm_his_doctor` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `true_name` varchar(20) NOT NULL DEFAULT '' COMMENT '用户个人资料真实姓名',
  `age` int(3) DEFAULT '0' COMMENT '年龄',
  `picture` varchar(255) DEFAULT '' COMMENT '头像',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别 0,空1:男  2:女',
  `background` tinyint(1) NOT NULL DEFAULT '0' COMMENT '学历 1：专科  2：本科  3：研究生  4：博士  5：博士后',
  `phone` varchar(11) NOT NULL DEFAULT '0' COMMENT '手机号',
  `mailbox` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `strong` varchar(255) NOT NULL DEFAULT '' COMMENT '擅长',
  `honor` varchar(255) NOT NULL DEFAULT '' COMMENT '荣誉',
  `introduction` text NOT NULL COMMENT '简介',
  `create_time` date COMMENT '注册时间',
  `update_time` date COMMENT '修改时间',
  `uid` int(11) NOT NULL COMMENT '用户表userid',
	`department_id` int(11) DEFAULT '0' COMMENT '科室id',
  `rank` tinyint(2) DEFAULT '0' COMMENT '医生级别 0:其他  1:主治医师  2:副主任医师  3:主任医师  4:医士  5:医师  6:助理医师  7:实习医师    8:主管护师  9:护师  10:护士  11:医师助理  12:研究生  13:随访员 ',
  `ask_price` decimal(10,2) DEFAULT '0.00' COMMENT '咨询价格',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='医生基本信息表';

-- ----------------------------
-- Records of dzm_his_doctor
-- ----------------------------
INSERT INTO `dzm_his_doctor` VALUES ('1', '张三', '25', '', '1', '0', '0', '', '', '', '1', null, null, '1', '1', '1', '0.00');
INSERT INTO `dzm_his_doctor` VALUES ('2', '李四', '26', '', '1', '0', '0', '', '1234', '', '1', null, null, '2', '1', '2', '0.00');
INSERT INTO `dzm_his_doctor` VALUES ('3', '王五', '828', '', '1', '0', '0', '', '', '', '1', null, null, '3','2', '2', '0.00');
INSERT INTO `dzm_his_doctor` VALUES ('4', '李狗蛋', '27', '1', '1', '0', '0', '1', '1', '1', '1', null, null, '4', '4', '1', '1.00');
INSERT INTO `dzm_his_doctor` VALUES ('5', 'aa', '25', '1', '1', '0', '0', '1', '1', '1', '1', null, null, '5','1', '12', '1.00');

DROP TABLE IF EXISTS `dzm_his_registeredfee`;
CREATE TABLE `dzm_his_registeredfee` (
  `reg_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL COMMENT '用户id',
  `company_id` int(11) NOT NULL COMMENT '公司ID',
  `registeredfee_name` varchar(255) NOT NULL COMMENT '挂号费用名称',
  `registeredfee_fee` decimal(8,2) unsigned NOT NULL COMMENT '金额',
  `registeredfee_sub_fee` decimal(8,2) unsigned NOT NULL COMMENT '子费用总数',
  `registeredfee_aggregate_amount` decimal(8,2) unsigned NOT NULL COMMENT '挂号费用总金额',
  `numberOfSub` int(5) NOT NULL COMMENT '子费用数量',
	`sub_registeredfee_name` varchar(255) NOT NULL COMMENT '挂号费用子名称',
  `sub_registeredfee_fee` decimal(8,2) NOT NULL COMMENT '子费用 ',
  `create_time` date COMMENT '创建时间',
  PRIMARY KEY (`reg_id`),
  KEY `mid` (`mid`) USING BTREE,
  KEY `company_id` (`company_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='挂号费用表';

-- ----------------------------
-- Records of dzm_his_registeredfee
-- ----------------------------
INSERT INTO `dzm_his_registeredfee` VALUES ('1', '1', '1', '急诊', '10.00', '3.00', '13.00', '1','病本', '3.00', null);
INSERT INTO `dzm_his_registeredfee` VALUES ('2', '1', '1', '门诊', '5.00', '2.00', '7.00', '1','包装', '2.00', null);
INSERT INTO `dzm_his_registeredfee` VALUES ('3', '1', '1', '专家诊', '20.00', '0.00', '20.00', '0','好', '10.00', null);