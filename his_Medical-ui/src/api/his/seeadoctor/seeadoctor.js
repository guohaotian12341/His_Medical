import request from '@/utils/request'

// 查询历史病历详细
export function getHistoryList(patientId) {
  return request({
    url: '/sickness/history/query/'+patientId,
    method: 'get',
  })
}

// 查询历史病历详细
export function getHistory(id) {
  return request({
    url: '/sickness/history/'+id,
    method: 'get',
  })
}

//修改历史病
export function upHistory(query) {
  return request({
    url: '/sickness/history',
    method: 'put',
    params: query
  })
}
  
// 查询门诊挂号详细
export function getRegistration(registrationId) {
  return request({
    url: '/sickness/registration/' + registrationId,
    method: 'get'
  })
}

// 查询患者用户列表
export function listPatient(data) {
  return request({
    url: '/sickness/patient/list',
    method: 'get',
    params: data
  })
}


// 查询门诊挂号列表
export function listRegistration(query) {
  return request({
    url: '/sickness/registration/list',
    method: 'get',
    params: query
  })
}

//查询药品列表
export function getDrug(query) {
  return request({
    url: '/sickness/prescription/druglist',
    method: 'get',
    params: query
  })
}

//查询检查列表
export function getExamine(query) {
  return request({
    url: '/sickness/prescription/examinelist',
    method: 'get',
    params: query
  })
}

//通过药品ID查询列表
export function getDrugall(medicinesIds) {
  return request({
    url: '/sickness/prescription/drug/'+medicinesIds,
    method: 'get',
  })
}

//通过药品ID查询列表
export function getExamineall(insIds) {
  return request({
    url: '/sickness/prescription/examine/'+insIds,
    method: 'get',
  })
}

//收费总表新增
export function addPkg(data) {
  return request({
    url: '/system/pkg',
    method: 'post',
    data: data
  })
}

// 新增处方列
export function addOrder(data) {
  return request({
    url: '/sickness/order',
    method: 'post',
    data: data
  })
}

// 新增开诊用药明细||检查项目
export function addSub(data) {
  return request({
    url: '/sickness/sub',
    method: 'post',
    data: data
  })
}

// 改为已就诊状态门诊挂号
export function alterHisHomeVisit(registrationId) {
  return request({
    url: '/sickness/registration/update/' + registrationId,
    method: 'put'
  })
}