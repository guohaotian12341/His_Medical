import request from '@/utils/request'

// 查询岗位列表
export function listPost(query) {
  return request({
    url: '/his/dzmpatient/list',
    method: 'get',
    params: query
  })
}

export function getCache() {
  return request({
    url: '/his/dzmeachrts/amount',
    method: 'get'
  })
}

export function getCaches() {
  return request({
    url: '/his/dzmeachrts/sf',
    method: 'get'
  })
}
