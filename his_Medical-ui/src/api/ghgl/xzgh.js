import request from '@/utils/request'


export function list(query) {
  return request({
    url: '/ghgl/xzgh/list',
    method: 'get',
    params: query
  })
}
export function dept() {
  return request({
    url: '/ghgl/xzgh/dept',
    method: 'get',
    
  })
}
export function gh(query) {
  return request({
    url: '/ghgl/xzgh/gh',
    method: 'get',
    params: query
  })
}
export function qrgh(query) {
  return request({
    url: '/ghgl/xzgh/qrgh',
    method: 'get',
    params: query
  })
}

  export function addpatients(data) {
    return request({
      url: '/ghgl/xzgh',
      method: 'post',
      params: data
    })

}