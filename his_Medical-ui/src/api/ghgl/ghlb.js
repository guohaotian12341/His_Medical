import request from '@/utils/request'

// 查询门诊挂号列表
export function listRegistration(query) {
    return request({
      url: '/ghgl/ghlb/list',
      method: 'get',
      params: query
    })
}
//退号
export function tuihao(registration_number) {
  return request({
    url: '/ghgl/ghlb/tuihao?registration_number='+registration_number,
    method: 'put'
    
  })
}
//作废
export function zuofei(registration_number) {
  return request({
    url: '/ghgl/ghlb/zuofei?registration_number='+registration_number,
    method: 'put'
    
  })
}
export function fukuan(registration_number) {
  return request({
    url: '/ghgl/ghlb/fukuan?registration_number='+registration_number,
    method: 'put'
    
  })
}

