import request from '@/utils/request'


// 查询药品息列表
export function medicineslist(arrs) {
  return request({
    url: '/purchase/querylist?arrs='+arrs,
    method: 'get',
    
  })
}

// 查询库存息列表
export function kcxx() {
  return request({
    url: '/his/inventory/list',
    method: 'get',
    
  })
}

// 新增采购信息
export function addpurchase(purchase) {
  return request({
    url: '/purchase/addpurchase',
    method: 'post',
    data: purchase
  })
}

//查询供应商
export function getsupplier() {
  return request({
    url: '/purchase/getSupplier',
    method: 'get'
  })
}
// 查询药品
export function getMedicines() {
  return request({
    url: '/purchase/queryMedicines',
    method: 'get'
  })
}

//生成的编号
export function createId() {
  return request({
    url: '/purchase/createid',
    method: 'get'
  })
}

// 新增批次库存
export function addbatchesinventory(inventoryinfo) {
  return request({
    url: '/purchase/addbatchesinventory',
    method: 'post',
    data: inventoryinfo
  })
}


// // 提交审核
// export function addbatchesinventory(inventoryinfo) {
//   return request({
//     url: '/purchase/addbatchesinventory',
//     method: 'post',
//     data: inventoryinfo
//   })
// }


// 直接入库
export function addbatchesinventoryto(inventoryinfo) {
  return request({
    url: '/purchase/addbatchesinventoryto',
    method: 'post',
    data: inventoryinfo
  })
}


// //暂存batchesinventoryST
// export function batchesinventoryST(inventoryinfo) {
//   return request({
//     url: '/purchase/batchesinventoryST',
//     method: 'post',
//     data: inventoryinfo
//   })
// }

//删除库存
export function delmedicines(inventoryId) {
    return request({
      url: '/his/inventory/del?inventoryId'+('='+inventoryId),
      method: 'get'
    })
  }





