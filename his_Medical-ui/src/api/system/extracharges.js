import request from '@/utils/request'

// 查询处方附加费用列表
export function listExtracharges(query) {
  return request({
    url: '/system/extracharges/list',
    method: 'get',
    params: query
  })
}

// 查询处方附加费用详细
export function getExtracharges(preId) {
  return request({
    url: '/system/extracharges/' + preId,
    method: 'get'
  })
}

// 新增处方附加费用
export function addExtracharges(data) {
  return request({
    url: '/system/extracharges',
    method: 'post',
    data: data
  })
}

// 修改处方附加费用
export function updateExtracharges(data) {
  return request({
    url: '/system/extracharges',
    method: 'put',
    data: data
  })
}

// 删除处方附加费用
export function delExtracharges(preId) {
  return request({
    url: '/system/extracharges/' + preId,
    method: 'delete'
  })
}
