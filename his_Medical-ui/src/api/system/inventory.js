import request from '@/utils/request'

// 查询药品信息列表
export function medilist(query) {
    return request({
      url: '/his/inventory/lists',
      method: 'get',
      params: query
    })
  }
  //删除药品
  export function delmedicines(batchesOfInventoryId) {
    return request({
      url: '/his/inventory/delss?batchesOfInventoryId'+('='+batchesOfInventoryId),
      method: 'get'
    })
  }
  //导入药品
  export function importTemplate() {
    return request({
      url: '/medicines/importTemplate',
      method: 'get'
    })
  }

// 修改菜单
export function updateMenu(data) {
  return request({
    url: '/his/inventory/update',
    method: 'put',
    data: data
  })
}


// 修改菜单
export function updateMenuss(data) {
  return request({
    url: '/his/inventory/updatess',
    method: 'put',
    data: data
  })
}
