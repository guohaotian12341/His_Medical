import request from '@/utils/request'

// 查询患者库列表
export function listPatient(query) {
  return request({
    url: '/system/patient/list',
    method: 'get',
    params: query
  })
}

// 查询患者详细
export function getPatient(patient_id) {
  return request({
    url: '/system/patient/'+patient_id,
    method: 'get',
  })
}

// 新增患者详细信息
export function addPatients(patientInfo) {
  return request({
    url: '/system/patient/addPatient',
    method: 'post',
    data: patientInfo
  })
}

// 修改患者信息
export function updatePatient(data) {
  return request({
    url: '/system/patient/updatePatient',
    method: 'put',
    data: data
  })
}

// 删除患者信息
export function delPatient(patient_id) {
  return request({
    url: '/system/patient/' + patient_id,
    method: 'delete'
  })
}

//状态修改
export function getupdatePafie(patient_id) {
  return request({
    url: '/system/patient/updatePafie/'+patient_id,
    method: 'put',
  })
}
export function getupPatient(patient_id) {
  return request({
    url: '/system/patient/upPatient/'+patient_id,
    method: 'put',
  })
}

//患者信息详细
export function getPatientall(patient_id) {
  return request({
    url: '/system/patient/LHH/'+patient_id,
    method: 'get',
  })
}