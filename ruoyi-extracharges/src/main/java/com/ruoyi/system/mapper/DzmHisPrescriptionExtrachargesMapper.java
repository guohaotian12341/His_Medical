package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DzmHisPrescriptionExtracharges;

/**
 * 处方附加费用Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-17
 */
public interface DzmHisPrescriptionExtrachargesMapper 
{
    /**
     * 查询处方附加费用
     * 
     * @param preId 处方附加费用主键
     * @return 处方附加费用
     */
    public DzmHisPrescriptionExtracharges selectDzmHisPrescriptionExtrachargesByPreId(String preId);

    /**
     * 查询处方附加费用列表
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 处方附加费用集合
     */
    public List<DzmHisPrescriptionExtracharges> selectDzmHisPrescriptionExtrachargesList(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges);

    /**
     * 新增处方附加费用
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 结果
     */
    public int insertDzmHisPrescriptionExtracharges(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges);

    /**
     * 修改处方附加费用
     * 
     * @param dzmHisPrescriptionExtracharges 处方附加费用
     * @return 结果
     */
    public int updateDzmHisPrescriptionExtracharges(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges);

    /**
     * 删除处方附加费用
     * 
     * @param preId 处方附加费用主键
     * @return 结果
     */
    public int deleteDzmHisPrescriptionExtrachargesByPreId(String preId);

    /**
     * 批量删除处方附加费用
     * 
     * @param preIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisPrescriptionExtrachargesByPreIds(String[] preIds);
}
