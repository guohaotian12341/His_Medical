package com.ruoyi.system.domain;

import java.math.BigDecimal;
import java.util.Date;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 处方附加费用对象 dzm_his_prescription_extracharges
 * 
 * @author ruoyi
 * @date 2022-06-17
 */
public class DzmHisPrescriptionExtracharges
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String preId;

    /** 添加用户id */
    @Excel(name = "添加用户id")
    private Long mid;

    /** 医院id */
    @Excel(name = "医院id")
    private Long hid;

    /** 处方附加费名称 */
    @Excel(name = "处方附加费名称")
    private String extrachargesName;

    /** 金额 */
    @Excel(name = "金额")
    private BigDecimal fee;

    /** 处方类型  0:中药处方  1:西药处方 */
    @Excel(name = "处方类型  0:中药处方  1:西药处方")
    private Integer type;

    /** 创建时间 */
    @Excel(name = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;

    /** 修改时间 */
    @Excel(name = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateTime;

    public void setPreId(String preId) 
    {
        this.preId = preId;
    }

    public String getPreId() 
    {
        return preId;
    }
    public void setMid(Long mid) 
    {
        this.mid = mid;
    }

    public Long getMid() 
    {
        return mid;
    }
    public void setHid(Long hid) 
    {
        this.hid = hid;
    }

    public Long getHid() 
    {
        return hid;
    }
    public void setExtrachargesName(String extrachargesName) 
    {
        this.extrachargesName = extrachargesName;
    }

    public String getExtrachargesName() 
    {
        return extrachargesName;
    }
    public void setFee(BigDecimal fee) 
    {
        this.fee = fee;
    }

    public BigDecimal getFee() 
    {
        return fee;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("preId", getPreId())
            .append("mid", getMid())
            .append("hid", getHid())
            .append("extrachargesName", getExtrachargesName())
            .append("fee", getFee())
            .append("type", getType())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
