package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.DzmHisInspectionfee;

/**
 * 检查项目费用Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
public interface DzmHisInspectionfeeMapper 
{
    /**
     * 查询检查项目费用
     * 
     * @param insId 检查项目费用主键
     * @return 检查项目费用
     */
    public DzmHisInspectionfee selectDzmHisInspectionfeeByInsId(String insId);

    /**
     * 查询检查项目费用列表
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 检查项目费用集合
     */
    public List<DzmHisInspectionfee> selectDzmHisInspectionfeeList(DzmHisInspectionfee dzmHisInspectionfee);

    /**
     * 新增检查项目费用
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 结果
     */
    public int insertDzmHisInspectionfee(DzmHisInspectionfee dzmHisInspectionfee);

    /**
     * 修改检查项目费用
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 结果
     */
    public int updateDzmHisInspectionfee(DzmHisInspectionfee dzmHisInspectionfee);

    /**
     * 删除检查项目费用
     * 
     * @param insId 检查项目费用主键
     * @return 结果
     */
    public int deleteDzmHisInspectionfeeByInsId(String insId);

    /**
     * 批量删除检查项目费用
     * 
     * @param insIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisInspectionfeeByInsIds(String[] insIds);
}
