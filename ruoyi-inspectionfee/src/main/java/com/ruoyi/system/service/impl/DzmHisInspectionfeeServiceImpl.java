package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisInspectionfeeMapper;
import com.ruoyi.system.domain.DzmHisInspectionfee;
import com.ruoyi.system.service.IDzmHisInspectionfeeService;

/**
 * 检查项目费用Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisInspectionfeeServiceImpl implements IDzmHisInspectionfeeService 
{
    @Autowired
    private DzmHisInspectionfeeMapper dzmHisInspectionfeeMapper;

    /**
     * 查询检查项目费用
     * 
     * @param insId 检查项目费用主键
     * @return 检查项目费用
     */
    @Override
    public DzmHisInspectionfee selectDzmHisInspectionfeeByInsId(String insId)
    {
        return dzmHisInspectionfeeMapper.selectDzmHisInspectionfeeByInsId(insId);
    }

    /**
     * 查询检查项目费用列表
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 检查项目费用
     */
    @Override
    public List<DzmHisInspectionfee> selectDzmHisInspectionfeeList(DzmHisInspectionfee dzmHisInspectionfee)
    {
        return dzmHisInspectionfeeMapper.selectDzmHisInspectionfeeList(dzmHisInspectionfee);
    }

    /**
     * 新增检查项目费用
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 结果
     */
    @Override
    public int insertDzmHisInspectionfee(DzmHisInspectionfee dzmHisInspectionfee)
    {
        return dzmHisInspectionfeeMapper.insertDzmHisInspectionfee(dzmHisInspectionfee);
    }

    /**
     * 修改检查项目费用
     * 
     * @param dzmHisInspectionfee 检查项目费用
     * @return 结果
     */
    @Override
    public int updateDzmHisInspectionfee(DzmHisInspectionfee dzmHisInspectionfee)
    {
        return dzmHisInspectionfeeMapper.updateDzmHisInspectionfee(dzmHisInspectionfee);
    }

    /**
     * 批量删除检查项目费用
     * 
     * @param insIds 需要删除的检查项目费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisInspectionfeeByInsIds(String[] insIds)
    {
        return dzmHisInspectionfeeMapper.deleteDzmHisInspectionfeeByInsIds(insIds);
    }

    /**
     * 删除检查项目费用信息
     * 
     * @param insId 检查项目费用主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisInspectionfeeByInsId(String insId)
    {
        return dzmHisInspectionfeeMapper.deleteDzmHisInspectionfeeByInsId(insId);
    }
}
