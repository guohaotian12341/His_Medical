package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("dzm_his_care_order_sub")
public class Dzmcareordersub {
    //所属开诊ID
    @Excel(name = "所属开诊ID")
    private Integer fid;
    //药品名
    @Excel(name = "药品名")
    private String good;
    //单位
    @Excel(name = "单位")
    private String unit;
    //单价
    @Excel(name = "单价")
    private String price;
    //用量
    @Excel(name = "用量")
    private String num;
    //金额
    @Excel(name = "金额")
    private Double amount;
    //备注
    @Excel(name = "备注")
    private String tips;
}
