package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
@TableName("dzm_his_inspectionfee")
public class Dzminspectionfee {
       @TableId(value = "insId")
       //项目名称
       @Excel(name = "项目名称")
       private String name;
       //项目类别
       @Excel(name = "项目类别")
       private String clas;
       //项目单价
       @Excel(name = "项目单价")
       private Double price;
       //项目成本
       @Excel(name = "项目成本")
       private Double cost;
       //创建时间
       @Excel(name = "创建时间")
       private String createtime;
       //修改时间
       @Excel(name = "修改时间")
       private Integer updatetime;
       //单位
       @Excel(name = "单位")
       private String unit;
}
