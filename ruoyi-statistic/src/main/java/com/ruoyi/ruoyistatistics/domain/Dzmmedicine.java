package com.ruoyi.ruoyistatistics.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class Dzmmedicine {
    //药品名称
    @Excel(name = "药品名称")
    private String name;
    //药品类型
    @Excel(name = "药品类型")
    private String classs;
    //药品用量
    @Excel(name = "药品用量")
    private String unit;
    //药品数量
    @Excel(name = "药品数量")
    private Integer num;
    //批发单价
    @Excel(name = "批发单价")
    private Double prices;
    //批发总额
    @Excel(name = "批发总额")
    private Double amounts;
    //销售单价
    @Excel(name = "销售单价")
    private Double price;
    //销售总额
    @Excel(name = "销售总额")
    private Double amount;
}
