package com.ruoyi.ruoyistatistics.domain;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class Dzmhisdoctorses {
       //医生姓名
       @Excel(name = "医生姓名")
       private String truename;
       //就诊人数
       @Excel(name = "就诊人数")
       private String jzrs;
       //挂号实收
       @Excel(name = "挂号实收")
       private Double ghss;
       //挂号总费用
       @Excel(name = "挂号总费用")
       private Double ghzfy;
       //子挂号费用
       @Excel(name = "子挂号费用")
       private Double zghfy;
       //总收入
       @Excel(name = "总收入")
       private Double zsr;
}
