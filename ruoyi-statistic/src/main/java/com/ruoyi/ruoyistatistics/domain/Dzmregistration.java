package com.ruoyi.ruoyistatistics.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
@TableName("dzm_his_registration")
public class Dzmregistration {
       @TableId("registration_id")
       //门诊挂号ID
       private Integer registrationId;
       //挂号编号
       @Excel(name = "挂号编号")
       private String number;
       //用户姓名
       @Excel(name = "用户姓名")
       private String name;
       //用户性别
       @Excel(name = "用户性别")
       private String sex;
       //科室名称
       @Excel(name = "科室名称")
       private String department;
       //医生姓名
       @Excel(name = "医生姓名")
       private String truename;
       //挂号总金额
       @Excel(name = "挂号总金额")
       private Double amount;
       //挂号状态
       @Excel(name = "挂号状态")
       private Integer status;
       //挂号时间
       @Excel(name = "挂号时间")
       private Integer time;
}
