package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import com.ruoyi.ruoyistatistics.domain.Dzmregistration;
import com.ruoyi.ruoyistatistics.service.IDzmregistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/his/dzmregistration")
public class DzmregistrationHandler extends BaseController {
    @Autowired
    private IDzmregistrationService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/find")
    public Object Find(Dzmregistration dzmregistration){
         startPage();
        List<Dzmregistration> Find = service.Find(dzmregistration);
        return getDataTable(Find);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/exports")
    public void export(HttpServletResponse response, Dzmregistration dzmregistration)
    {
        List<Dzmregistration> list = service.Find(dzmregistration);
        ExcelUtil<Dzmregistration> util = new ExcelUtil<Dzmregistration>(Dzmregistration.class);
        util.exportExcel(response, list, "定时任务");
    }
}
