package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.ruoyistatistics.domain.Dzmcareordersub;
import com.ruoyi.ruoyistatistics.service.IDzmcareordersubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/his/dzmcareordersub")
public class DzmcareordersubHandler extends BaseController {
    @Autowired
    private IDzmcareordersubService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("sub")
    public Object order(Dzmcareordersub dzmcareordersub){
        startPage();
        List<Dzmcareordersub> sub = service.sub(dzmcareordersub);
        return getDataTable(sub);
    }
}
