package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.ruoyistatistics.domain.Dzmhisdoctorses;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import com.ruoyi.ruoyistatistics.service.IDzmhisdoctorsesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/his/dzmhisdoctor")
public class DzmhisdoctorsesHandler extends BaseController {
    @Autowired
    private IDzmhisdoctorsesService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/doc")
    public Object order(Dzmhisdoctorses dzmhisdoctor){
        startPage();
        System.out.println("---------------"+dzmhisdoctor.getTruename());
        List<Dzmhisdoctorses> doc = service.doc(dzmhisdoctor);
        return getDataTable(doc);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Dzmhisdoctorses dzmhisdoctorses)
    {
        List<Dzmhisdoctorses> list = service.doc(dzmhisdoctorses);
        ExcelUtil<Dzmhisdoctorses> util = new ExcelUtil<Dzmhisdoctorses>(Dzmhisdoctorses.class);
        util.exportExcel(response, list, "定时任务");
    }

}
