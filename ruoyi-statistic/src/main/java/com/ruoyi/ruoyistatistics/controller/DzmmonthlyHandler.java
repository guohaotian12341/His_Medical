package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.ruoyistatistics.domain.Dzmmonthly;
import com.ruoyi.ruoyistatistics.service.impl.IDzmmonthlyServiceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("his/dzmmonthly")
public class DzmmonthlyHandler extends BaseController {
     @Autowired
     private IDzmmonthlyServiceimpl service;

     @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
     @GetMapping("/ydtj")
     public Object ydtj(Dzmmonthly dzmmonthly){
          startPage();
          List<Dzmmonthly> amount = service.ydtj(dzmmonthly);
          return getDataTable(amount);

     }

}
