package com.ruoyi.ruoyistatistics.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.ruoyistatistics.domain.Dzmecharts;
import com.ruoyi.ruoyistatistics.service.IDzmechartsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/his/dzmeachrts")
public class DzmechartsHandler extends BaseController {
    @Autowired
    private IDzmechartsService service;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/amount")
    public Object amount(Dzmecharts dzmecharts){
        startPage();
        List<Dzmecharts> amount = service.amount(dzmecharts);
        return getDataTable(amount);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('accountant') || @ss.hasPermi('cashier')")
    @GetMapping("/sf")
    public Object sf(Dzmecharts dzmecharts){
        startPage();
        List<Dzmecharts> sf = service.sf(dzmecharts);
        return getDataTable(sf);
    }
}
