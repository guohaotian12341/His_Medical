package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmregisteredfee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(value = DataSourceType.SLAVE)
public interface IDzmRegisteredFeeMapper extends BaseMapper<Dzmregisteredfee> {
       List<Dzmregisteredfee> findAll(Dzmregisteredfee dzmregisteredfee);
}
