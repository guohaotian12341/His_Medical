package com.ruoyi.ruoyistatistics.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@DataSource(value = DataSourceType.SLAVE)
@Mapper
public interface IDzmPatientMapper extends BaseMapper<Dzmpatient> {
    List<Dzmpatient> sel(Dzmpatient dzmpatient);

    List<Dzmpatient> amount(Dzmpatient dzmpatient);
}
