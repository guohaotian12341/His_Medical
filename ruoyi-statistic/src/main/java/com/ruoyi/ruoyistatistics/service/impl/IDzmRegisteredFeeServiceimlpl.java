package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmregisteredfee;
import com.ruoyi.ruoyistatistics.mapper.IDzmRegisteredFeeMapper;
import com.ruoyi.ruoyistatistics.service.IDzmRegisteredFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmRegisteredFeeServiceimlpl extends ServiceImpl<IDzmRegisteredFeeMapper, Dzmregisteredfee> implements IDzmRegisteredFeeService {
    @Autowired
    private IDzmRegisteredFeeMapper mapper;

    @Override
    public List<Dzmregisteredfee> findAll(Dzmregisteredfee dzmregisteredfee) {
        return mapper.findAll(dzmregisteredfee);
    }
}
