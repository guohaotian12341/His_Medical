package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmmedicine;

import java.util.List;

public interface IDzmMedicineService extends IService<Dzmmedicine> {
    List<Dzmmedicine> all(Dzmmedicine dzmmedicine);
}
