package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzminspectionfee;

import java.util.List;

public interface IDzmInspectionfeeService extends IService<Dzminspectionfee> {
         List<Dzminspectionfee> sles(Dzminspectionfee dzminspectionfee);
}
