package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;

import java.util.List;

public interface IDzmPatientService extends IService<Dzmpatient> {
          List<Dzmpatient> sel(Dzmpatient dzmpatient);

          List<Dzmpatient> amount(Dzmpatient dzmpatient);
}
