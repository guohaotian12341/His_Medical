package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.domain.Dzmcareorder;
import com.ruoyi.ruoyistatistics.mapper.IDzmcareorderMapper;
import com.ruoyi.ruoyistatistics.service.IDzmcareorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmcareorderServiceimpl extends ServiceImpl<IDzmcareorderMapper, Dzmcareorder> implements IDzmcareorderService {
    @Autowired
    private IDzmcareorderMapper mapper;

    @Override
    public List<Dzmcareorder> queryList(Dzmcareorder dzmcareorder) {
        return mapper.queryList(dzmcareorder);
    }

    @Override
    public List<Dzmcareorder> orders(Dzmcareorder dzmcareorders) {
        return mapper.orders(dzmcareorders);
    }

    @Override
    public Dzmcareorder Patient(Integer id) {
        return mapper.Patient(id);
    }

    @Override
    public List<Dzmcareorder> prescriPtion(Integer id) {
        return mapper.prescriPtion(id);
    }

    @Override
    public Integer upStatus(Dzmcareorder dzmHisCarePkg) {
        return mapper.upStatus(dzmHisCarePkg);
    }

    @Override
    public Integer paytheFees(Dzmcareorder dzmHisCarePkg) {
        return mapper.paytheFees(dzmHisCarePkg);
    }

    @Override
    public Integer reiMburse(Dzmcareorder dzmHisCarePkg) {
        return mapper.reiMburse(dzmHisCarePkg);
    }

    @Override
    public List<Dzmcareorder> selectdzmPkg(Dzmcareorder dzmHisCarePkg) {
        return mapper.selectdzmPkg(dzmHisCarePkg);
    }
}
