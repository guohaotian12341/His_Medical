package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmecharts;

import java.util.List;

public interface IDzmechartsService extends IService<Dzmecharts> {
    List<Dzmecharts> amount(Dzmecharts dzmecharts);

    List<Dzmecharts> sf(Dzmecharts dzmecharts);
}
