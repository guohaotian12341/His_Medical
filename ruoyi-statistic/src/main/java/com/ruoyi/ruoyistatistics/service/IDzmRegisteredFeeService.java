package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmregisteredfee;

import java.util.List;

public interface IDzmRegisteredFeeService extends IService<Dzmregisteredfee> {
      List<Dzmregisteredfee> findAll(Dzmregisteredfee dzmregisteredfee);
}
