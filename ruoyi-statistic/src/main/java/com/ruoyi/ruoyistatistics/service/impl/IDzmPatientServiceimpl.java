package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.mapper.IDzmPatientMapper;
import com.ruoyi.ruoyistatistics.service.IDzmPatientService;
import com.ruoyi.ruoyistatistics.domain.Dzmpatient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmPatientServiceimpl extends ServiceImpl<IDzmPatientMapper, Dzmpatient> implements IDzmPatientService {
      @Autowired
    private IDzmPatientMapper mapper;

    @Override
    public List<Dzmpatient> sel(Dzmpatient dzmpatient) {
        return mapper.sel(dzmpatient);
    }

    @Override
    public List<Dzmpatient> amount(Dzmpatient dzmpatient) {
        return mapper.amount(dzmpatient);
    }
}
