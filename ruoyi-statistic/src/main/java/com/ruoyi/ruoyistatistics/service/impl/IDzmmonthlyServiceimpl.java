package com.ruoyi.ruoyistatistics.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.ruoyistatistics.controller.DzmmonthlyHandler;
import com.ruoyi.ruoyistatistics.domain.Dzmmonthly;
import com.ruoyi.ruoyistatistics.mapper.IDzmmedicineMapper;
import com.ruoyi.ruoyistatistics.mapper.IDzmmonthlyMapper;
import com.ruoyi.ruoyistatistics.service.IDzmmonthlyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class IDzmmonthlyServiceimpl extends ServiceImpl<IDzmmonthlyMapper,Dzmmonthly> implements IDzmmonthlyService {
    @Autowired
    private IDzmmonthlyMapper mapper;


    @Override
    public List<Dzmmonthly> ydtj(Dzmmonthly dzmmonthly) {
        return mapper.ydtj(dzmmonthly);
    }
}
