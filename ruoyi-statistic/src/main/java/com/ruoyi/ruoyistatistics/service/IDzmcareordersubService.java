package com.ruoyi.ruoyistatistics.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.ruoyistatistics.domain.Dzmcareordersub;

import java.util.List;

public interface IDzmcareordersubService extends IService<Dzmcareordersub> {
         List<Dzmcareordersub> sub(Dzmcareordersub dzmcareordersub);
}
