package com.ruoyi.registration.service.impl;


import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.registration.domain.Registration;
import com.ruoyi.registration.mapper.IRegistrationMapper;
import com.ruoyi.registration.service.IRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("Registration")
@DataSource(DataSourceType.SLAVE)
public class RegistrationServiceImpl implements IRegistrationService {
    @Autowired
    private IRegistrationMapper mapper;
    @Override
    public List<Registration> list(Registration reg) {
        List<Registration> list = mapper.list(reg);

        return list;
    }

    @Override
    public Integer tuihao(String registration_number) {
        return mapper.tuihao(registration_number);
    }

    @Override
    public Integer zuofei(String registration_number) {
        return mapper.zuofei(registration_number);
    }

    @Override
    public Integer fukuan(String registration_number) {
        return mapper.fukuan(registration_number);
    }


}
