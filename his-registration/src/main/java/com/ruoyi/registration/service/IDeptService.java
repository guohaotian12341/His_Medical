package com.ruoyi.registration.service;

import com.ruoyi.registration.domain.DeptVo;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IDeptService {
    public List<DeptVo> list();
    public List<DeptVo> bg(DeptVo deptVo);
    public Integer qrgh(DeptVo deptVo);
    int addHistory(DeptVo deptVo);

    int addPkg(DeptVo deptVo);
    int upsdatecheduling(DeptVo deptVo);

}
