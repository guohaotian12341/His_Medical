package com.ruoyi.registration.service;

import com.ruoyi.registration.domain.Registration;

import java.util.List;

public interface IRegistrationService{
    public List<Registration> list(Registration reg);
    public Integer tuihao(String registration_number);
    public Integer zuofei(String registration_number);
    public Integer fukuan(String registration_number);

}
