package com.ruoyi.registration.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Alias("reg")
@Data
public class Registration {
  private Integer registrationId ;//bigint unsigned NOT NULL AUTOINCREMENT,
  private Integer   patientId ;//int NOT NULL COMMENT '患者ID',
  private Integer  physicianId;// int NOT NULL COMMENT '医生ID',
  private Integer  operatorId;// int NOT NULL COMMENT '操作员ID',
  private Integer   companyId ;//int NOT NULL COMMENT '诊所ID',
  private Integer   departmentId; //int NOT NULL COMMENT '科室ID',
  private Integer   registeredfeeId; //int NOT NULL COMMENT '挂号费用ID',
  private Integer   registrationAmount;// float(8,2) NOT NULL COMMENT '挂号总金额',
  private String  registrationNumber;// bigint NOT NULL COMMENT '挂号编号',
  private Integer   registrationStatus; //tinyint NOT NULL DEFAULT '1' COMMENT '挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付',
  private Integer  schedulingId; //int NOT NULL COMMENT '排班主表ID',
  private Integer schedulingSubsectionId; //int NOT NULL COMMENT '排班时段表ID',
  private Integer   schedulingWeekId ;//int NOT NULL COMMENT '排班星期表ID',
  private String createTime; //int NOT NULL,
  private Integer   updateTime ;//int NOT NULL COMMENT '更新时间',
  private Integer  pkgId ; //int unsigned DEFAULT '0' COMMENT '收费总表care_pkg.id',
  private String departmentName;
  private String trueName;
  private String name;
  private String userName;
  private String registeredfeeName;
}
