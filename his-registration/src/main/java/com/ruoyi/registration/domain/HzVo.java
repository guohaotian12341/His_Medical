package com.ruoyi.registration.domain;

import lombok.Data;

@Data
public class HzVo {
    private Integer patientId;
    private String name;
    private Integer sex;
    private  String caseCode;
    private String birthday;
    private String idCard;
    private Integer typeId;

}
