package com.ruoyi.registration.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.registration.domain.Registration;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
@DataSource(DataSourceType.SLAVE)
public interface IRegistrationMapper{
    public List<Registration> list(Registration reg);
    public Integer tuihao(String registration_number);
    public Integer zuofei(String registration_number);
    public Integer fukuan(String registration_number);
}
