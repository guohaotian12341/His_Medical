package com.ruoyi.registration.mapper;

import com.ruoyi.registration.domain.DeptVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface IDeptMapper {
    public List<DeptVo> list();
    public List<DeptVo> bg(DeptVo deptVo);
    public Integer qrgh(DeptVo deptVo);
    int addHistory(DeptVo deptVo);

    int addPkg(DeptVo deptVo);
    int upsdatecheduling(DeptVo deptVo);
}
