package com.ruoyi.registration.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.core.domain.entity.DzmHisScheduling;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ISchedulingMapper{
    public int insert(DzmHisScheduling scheduling);


    /**
     * 查询我的排班列表
     *
     * @param dzmHisScheduling 我的排班
     * @return 我的排班集合
     */
    public List<DzmHisScheduling> list(DzmHisScheduling dzmHisScheduling);
    public List<DzmHisScheduling> alllist(DzmHisScheduling dzmHisScheduling);
    public List<DzmHisScheduling> wdpb(DzmHisScheduling dzmHisScheduling);
    public DzmHisScheduling getScheduling(Integer scheduling_id);
    int upScheduling(DzmHisScheduling scheduling);


}
