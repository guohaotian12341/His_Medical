package com.ruoyi.registration.mapper;

import com.ruoyi.registration.domain.HzVo;
import com.ruoyi.registration.domain.Patients;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IHzMapper {
    public List<HzVo> list(HzVo hzVo);
    public Integer  insertPatients(Patients patients);

}
