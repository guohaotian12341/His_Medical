package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
@Data
//@TableName("dzm_patient")
public class PatientInfo {
    @TableId
    @Excel(name = "编号",cellType = Excel.ColumnType.NUMERIC)
    private Integer patientId;// int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
    private Integer hospitalId;// int(10) DEFAULT '0' COMMENT '所属医院、诊所',
    @Excel(name = "患者姓名")
    private String name;// varchar(50) NOT NULL DEFAULT '',
    private String openid;// varchar(80) DEFAULT '0' COMMENT '微信openid',
    @Excel(name = "患者电话")
    private String mobile;// varchar(11) NOT NULL DEFAULT '' COMMENT '患者电话',
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @Excel(name = "修改时间")
    private Long updateTime;// date COMMENT '修改时间',
    private String password;// varchar(60) DEFAULT '' COMMENT ' 登录密码',
    @Excel(name = "患者性别", readConverterExp = "1=男,2=女")
    private Integer sex;// tinyint(2) DEFAULT '0' COMMENT '患者性别1男2女',
    private String birthday;// varchar(50) DEFAULT NULL,
    private String idCard;// char(18) DEFAULT NULL,
    private String mobile1;// char(11) DEFAULT NULL,
    @Excel(name = "是否完善信息",readConverterExp = "0=否,1=已完善")
    private Integer isFinal;// tinyint(4) DEFAULT '0' COMMENT '是否完善信息，0否1已完善',
    private Integer lastLoginIp;// int(10) DEFAULT '0' COMMENT '最后登录ip',
    private Long lastLoginTime;// date COMMENT '最后登录时间',
    @Excel(name = "地址信息")
    private String address;// varchar(120) DEFAULT NULL COMMENT '地址信息',
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;// date COMMENT '注册时间',
    private String provinceId;// int(11) DEFAULT '0' COMMENT '省区id',
    private String cityId;// int(11) DEFAULT '0' COMMENT '市区id',
    private String districtId;// int(11) DEFAULT '0' COMMENT '县区id',
    private String allergyInfo;// varchar(100) DEFAULT NULL COMMENT '过敏信息',
    @Excel(name = "是否移除",readConverterExp = "0=正常,1=删除")
    private Integer isDel;// tinyint(1) DEFAULT '0' COMMENT '是否移除 0：正常 1：删除',

}
