package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.PafileInfo;
import com.ruoyi.system.domain.PatientInfo;
import com.ruoyi.system.mapper.LPatientMapper;
import com.ruoyi.system.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("LPatientServiceImpl")
@DataSource(value = DataSourceType.SLAVE)
public class PatientServiceImpl extends ServiceImpl<LPatientMapper, PatientInfo> implements PatientService {

    @Autowired
    private LPatientMapper mapper;
    @Override
    public List<PatientInfo> querylist(PatientInfo pi) {
        return mapper.querylist(pi);
    }

    @Override
    public PafileInfo filelist(Integer patientId) {
        return mapper.filelist(patientId);
    }

    @Override
    public Integer updatePatient(PatientInfo patientInfo) {
        System.out.println(patientInfo+"<<<");
        return mapper.updatePatient(patientInfo);
    }

    @Override
    public Integer deletePatient(Integer patientId) {
        return mapper.deletePatient(patientId);
    }

    @Override
    public Integer addPatient(PafileInfo pf) {
        return mapper.addPatient(pf);
    }

    @Override
    public PatientInfo All(Integer patientId) {
        return mapper.All(patientId);
    }

    @Override
    public List<PatientInfo> selectPatientList(PatientInfo patientInfo) {
        return mapper.selectPatientList(patientInfo);
    }

    @Override
    public Integer updatePafie(long patientId) {
        return mapper.updatePafie(patientId);
    }

    @Override
    public Integer upPatient(long patientId) {
        return mapper.upPatient(patientId);
    }


}
