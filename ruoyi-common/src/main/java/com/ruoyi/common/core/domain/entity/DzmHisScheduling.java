package com.ruoyi.common.core.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 我的排班对象 dzm_his_scheduling
 * 
 * @author ruoyi
 * @date 2022-06-23
 */

public class DzmHisScheduling extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String schedulingId;

    /** 医生ID */
    @Excel(name = "医生ID")
    private Long physicianid;

    /** 排班类型 1-普通医生   2-主任医生 */
    @Excel(name = "排班类型 1-普通医生   2-主任医生")
    private Long schedulingType;

    /** 科室ID */
    @Excel(name = "科室ID")
    private Long departmentId;

    /** 诊所ID */
    @Excel(name = "诊所ID")
    private Long companyId;

    /** 排班日期 */
    @Excel(name = "排班日期")
    private String subsectionDate;

    /** 每天的时段：上午：1；下午：2；晚上：3； */
    @Excel(name = "每天的时段：上午：1；下午：2；晚上：3；")
    private Long subsectionType;

    /** 每天的时间：8:00-12:00：1；14:30-17:30：2；17:30-8:00：3；8:30-11:30：4 */
    @Excel(name = "每天的时间：8:00-12:00：1；14:30-17:30：2；17:30-8:00：3；8:30-11:30：4")
    private Long subsectionTime;

    /** 号源数量 */
    @Excel(name = "号源数量")
    private Long registeredfeeNum;

    /** 挂号费用ID */
    @Excel(name = "挂号费用ID")
    private Long registeredfeeId;

    private String trueName;
    private String departmentName;
    private String registeredfeeName;


    public String getSchedulingId() {
        return schedulingId;
    }

    public void setSchedulingId(String schedulingId) {
        this.schedulingId = schedulingId;
    }

    public Long getPhysicianid() {
        return physicianid;
    }

    public void setPhysicianid(Long physicianid) {
        this.physicianid = physicianid;
    }

    public Long getSchedulingType() {
        return schedulingType;
    }

    public void setSchedulingType(Long schedulingType) {
        this.schedulingType = schedulingType;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getSubsectionDate() {
        return subsectionDate;
    }

    public void setSubsectionDate(String subsectionDate) {
        this.subsectionDate = subsectionDate;
    }

    public Long getSubsectionType() {
        return subsectionType;
    }

    public void setSubsectionType(Long subsectionType) {
        this.subsectionType = subsectionType;
    }

    public Long getSubsectionTime() {
        return subsectionTime;
    }

    public void setSubsectionTime(Long subsectionTime) {
        this.subsectionTime = subsectionTime;
    }

    public Long getRegisteredfeeNum() {
        return registeredfeeNum;
    }

    public void setRegisteredfeeNum(Long registeredfeeNum) {
        this.registeredfeeNum = registeredfeeNum;
    }

    public Long getRegisteredfeeId() {
        return registeredfeeId;
    }

    public void setRegisteredfeeId(Long registeredfeeId) {
        this.registeredfeeId = registeredfeeId;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getRegisteredfeeName() {
        return registeredfeeName;
    }

    public void setRegisteredfeeName(String registeredfeeName) {
        this.registeredfeeName = registeredfeeName;
    }

    @Override
    public String toString() {
        return "DzmHisScheduling{" +
                "schedulingId='" + schedulingId + '\'' +
                ", physicianid=" + physicianid +
                ", schedulingType=" + schedulingType +
                ", departmentId=" + departmentId +
                ", companyId=" + companyId +
                ", subsectionDate='" + subsectionDate + '\'' +
                ", subsectionType=" + subsectionType +
                ", subsectionTime=" + subsectionTime +
                ", registeredfeeNum=" + registeredfeeNum +
                ", registeredfeeId=" + registeredfeeId +
                ", trueName='" + trueName + '\'' +
                ", departmentName='" + departmentName + '\'' +
                ", registeredfeeName='" + registeredfeeName + '\'' +
                '}';
    }
}
