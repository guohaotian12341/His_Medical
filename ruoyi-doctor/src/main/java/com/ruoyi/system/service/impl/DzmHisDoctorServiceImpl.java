package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DzmHisDoctorMapper;
import com.ruoyi.system.domain.DzmHisDoctor;
import com.ruoyi.system.service.IDzmHisDoctorService;

/**
 * 医生基本信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class DzmHisDoctorServiceImpl implements IDzmHisDoctorService 
{
    @Autowired
    private DzmHisDoctorMapper dzmHisDoctorMapper;

    /**
     * 查询医生基本信息
     * 
     * @param id 医生基本信息主键
     * @return 医生基本信息
     */
    @Override
    public DzmHisDoctor selectDzmHisDoctorById(String id)
    {
        return dzmHisDoctorMapper.selectDzmHisDoctorById(id);
    }

    /**
     * 查询医生基本信息列表
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 医生基本信息
     */
    @Override
    public List<DzmHisDoctor> selectDzmHisDoctorList(DzmHisDoctor dzmHisDoctor)
    {
        return dzmHisDoctorMapper.selectDzmHisDoctorList(dzmHisDoctor);
    }

    /**
     * 新增医生基本信息
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 结果
     */
    @Override
    public int insertDzmHisDoctor(DzmHisDoctor dzmHisDoctor)
    {
        dzmHisDoctor.setCreateTime(DateUtils.getNowDate());
        return dzmHisDoctorMapper.insertDzmHisDoctor(dzmHisDoctor);
    }

    /**
     * 修改医生基本信息
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 结果
     */
    @Override
    public int updateDzmHisDoctor(DzmHisDoctor dzmHisDoctor)
    {
        dzmHisDoctor.setUpdateTime(DateUtils.getNowDate());
        return dzmHisDoctorMapper.updateDzmHisDoctor(dzmHisDoctor);
    }

    /**
     * 批量删除医生基本信息
     * 
     * @param ids 需要删除的医生基本信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisDoctorByIds(String[] ids)
    {
        return dzmHisDoctorMapper.deleteDzmHisDoctorByIds(ids);
    }

    /**
     * 删除医生基本信息信息
     * 
     * @param id 医生基本信息主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisDoctorById(String id)
    {
        return dzmHisDoctorMapper.deleteDzmHisDoctorById(id);
    }
}
