package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.DzmHisDoctor;

/**
 * 医生基本信息Service接口
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
public interface IDzmHisDoctorService 
{
    /**
     * 查询医生基本信息
     * 
     * @param id 医生基本信息主键
     * @return 医生基本信息
     */
    public DzmHisDoctor selectDzmHisDoctorById(String id);

    /**
     * 查询医生基本信息列表
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 医生基本信息集合
     */
    public List<DzmHisDoctor> selectDzmHisDoctorList(DzmHisDoctor dzmHisDoctor);

    /**
     * 新增医生基本信息
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 结果
     */
    public int insertDzmHisDoctor(DzmHisDoctor dzmHisDoctor);

    /**
     * 修改医生基本信息
     * 
     * @param dzmHisDoctor 医生基本信息
     * @return 结果
     */
    public int updateDzmHisDoctor(DzmHisDoctor dzmHisDoctor);

    /**
     * 批量删除医生基本信息
     * 
     * @param ids 需要删除的医生基本信息主键集合
     * @return 结果
     */
    public int deleteDzmHisDoctorByIds(String[] ids);

    /**
     * 删除医生基本信息信息
     * 
     * @param id 医生基本信息主键
     * @return 结果
     */
    public int deleteDzmHisDoctorById(String id);
}
