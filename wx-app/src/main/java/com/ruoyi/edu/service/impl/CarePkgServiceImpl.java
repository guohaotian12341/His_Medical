package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.CarePkgVo;
import com.ruoyi.edu.mapper.CarePkgMapper;
import com.ruoyi.edu.service.CarePkgIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePkgServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/30 16:01
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Service
@DataSource(DataSourceType.SLAVE)
public class CarePkgServiceImpl extends ServiceImpl<CarePkgMapper, CarePkgVo> implements CarePkgIService {
    @Autowired
    private CarePkgMapper mapper;

    @Override
    public Integer insertCarePkg(CarePkgVo carePkgVo) {
        return mapper.insert(carePkgVo);
    }

    /**
     * @Author:guohaotian
     * @Description:查询总收费表中未支付的收费、挂号信息
     * @Param:carePkgVo
     * @Return:java.util.List<com.ruoyi.edu.domain.CarePkgVo>
     * @Date:2022/7/1~14:24
     */
    @Override
    public List<CarePkgVo> queryCarePkgById(CarePkgVo carePkgVo) {
        return mapper.queryCarePkgById(carePkgVo);
    }

    /**
     * @Author:guohaotian
     * @Description:支付成功后改变订单状态
     * @Param:carePkgVo
     * @Return:java.lang.Integer
     * @Date:2022/7/1~14:29
     */
    @Override
    public Integer updateStatus(CarePkgVo carePkgVo) {
        UpdateWrapper<CarePkgVo> up=new UpdateWrapper<>();
        up.set("status",1);
        up.eq("id",carePkgVo.getId());
        return mapper.update(null,up);
    }

    /**
     * @Author:guohaotian
     * @Description:根据id去修改状态（退号）
     * @Param:carePkgVo
     * @Return:java.lang.Integer
     * @Date:2022/7/1~17:16
     */
    @Override
    public Integer updateBackStatus(CarePkgVo carePkgVo) {
        UpdateWrapper<CarePkgVo> up=new UpdateWrapper<>();
        up.eq("id",carePkgVo.getId());
        up.set("status","4");
        return mapper.update(null,up);
    }
}
