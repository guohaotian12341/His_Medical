package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.WxOpenIdVo;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxOpenIdIService
 * @package_Name: com.sykj.edu.service
 * @User: guohaotian
 * @Date: 2022/6/17 15:27
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface WxOpenIdIService extends IService<WxOpenIdVo> {
}
