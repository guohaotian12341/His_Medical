package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.CareReFundLogVo;
import com.ruoyi.edu.mapper.CareReFundLogMapper;
import com.ruoyi.edu.service.CareReFundLogIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareReFundLogServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/7/1 17:01
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class CareReFundLogServiceImpl extends ServiceImpl<CareReFundLogMapper, CareReFundLogVo> implements CareReFundLogIService {
    @Autowired
    private CareReFundLogMapper mapper;

    @Override
    public Integer insertReFundLog(CareReFundLogVo careReFundLogVo) {
        careReFundLogVo.setPaymentPlatform(1);
        careReFundLogVo.setStatus(1);
        Date date=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(date);
        careReFundLogVo.setAddtime(format);
        careReFundLogVo.setAdmMemo("挂号退号");
        return mapper.insert(careReFundLogVo);
    }
}
