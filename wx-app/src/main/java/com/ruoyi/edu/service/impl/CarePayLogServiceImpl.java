package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.CarePayLogVo;
import com.ruoyi.edu.mapper.CarePayLogMapper;
import com.ruoyi.edu.service.CarePayLogIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePayLogServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/30 16:11
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class CarePayLogServiceImpl extends ServiceImpl<CarePayLogMapper, CarePayLogVo> implements CarePayLogIService {
    @Autowired
    private CarePayLogMapper mapper;

    @Override
    public Integer insertCarePayLog(CarePayLogVo carePayLogVo) {
        return mapper.insert(carePayLogVo);
    }

    /**
     * @Author:guohaotian
     * @Description:收费总表支付完根据支付总表id改变收费记录状态
     * @Param:carePayLogVo
     * @Return:java.lang.Integer
     * @Date:2022/7/1~14:32
     */
    @Override
    public Integer updateStatus(CarePayLogVo carePayLogVo) {
        UpdateWrapper<CarePayLogVo> up=new UpdateWrapper<>();
        up.eq("pkg_id",carePayLogVo.getPkgId());
        up.set("payment_platform",'1');
        up.set("status",'1');
        return mapper.update(null,up);
    }
}
