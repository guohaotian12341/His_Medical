package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.CarePkgVo;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePkgIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/30 16:01
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface CarePkgIService extends IService<CarePkgVo> {
    public Integer insertCarePkg(CarePkgVo carePkgVo);
    public List<CarePkgVo> queryCarePkgById(CarePkgVo carePkgVo);
    public Integer  updateStatus(CarePkgVo carePkgVo);
    public Integer updateBackStatus(CarePkgVo carePkgVo);
}
