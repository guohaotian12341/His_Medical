package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.HisRegistrationVo;
import com.ruoyi.edu.mapper.HisRegistrationMapper;
import com.ruoyi.edu.service.HisRegistrationIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HisRegistrationServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/30 14:48
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Service
@DataSource(DataSourceType.SLAVE)
public class HisRegistrationServiceImpl extends ServiceImpl<HisRegistrationMapper, HisRegistrationVo> implements HisRegistrationIService {
    @Autowired
    private HisRegistrationMapper mapper;

    /**
     * @Author:guohaotian
     * @Description:修改挂号
     * @Param:hisRegistrationVo
     * @Return:java.lang.Integer
     * @Date:2022/6/30~15:35
     */
    @Override
    public Integer UpdateRegistration(HisRegistrationVo hisRegistrationVo) {
        return mapper.updateById(hisRegistrationVo);
    }

    /**
     * @Author:guohaotian
     * @Description:新增挂号
     * @Param:hisRegistrationVo
     * @Return:java.lang.Integer
     * @Date:2022/6/30~15:35
     */
    @Override
    public Integer CreateRegistration(HisRegistrationVo hisRegistrationVo) {
        return mapper.insert(hisRegistrationVo);
    }

    /**
     * @Author:guohaotian
     * @Description:根据患者id查询所有挂号信息
     * @Param:hisRegistrationVo
     * @Return:java.util.List<com.ruoyi.edu.domain.HisRegistrationVo>
     * @Date:2022/7/1~16:01
     */
    @Override
    public List<HisRegistrationVo> queryHisRegistrationByPatientId(HisRegistrationVo hisRegistrationVo) {
        return mapper.queryHisRegistrationByPatientId(hisRegistrationVo);
    }

    /**
     * @Author:guohaotian
     * @Description:根据患者id查询待就诊的挂号信息
     * @Param:hisRegistrationVo
     * @Return:java.util.List<com.ruoyi.edu.domain.HisRegistrationVo>
     * @Date:2022/7/1~16:50
     */
    @Override
    public List<HisRegistrationVo> queryHisRegistrationStatus(HisRegistrationVo hisRegistrationVo) {
        return mapper.queryHisRegistrationStatus(hisRegistrationVo);
    }

    /**
     * @Author:guohaotian
     * @Description:根据id修改状态（退号）
     * @Param:hisRegistrationVo
     * @Return:java.lang.Integer
     * @Date:2022/7/1~17:12
     */
    @Override
    public Integer updateStatus(HisRegistrationVo hisRegistrationVo) {
        UpdateWrapper<HisRegistrationVo> up=new UpdateWrapper<>();
        up.eq("registration_id",hisRegistrationVo.getRegistrationId());
        up.set("registration_status",'3');
        return mapper.update(null,up);
    }
}
