package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.CareReFundLogVo;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareReFundLogIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/7/1 17:01
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface CareReFundLogIService extends IService<CareReFundLogVo> {
    public Integer insertReFundLog(CareReFundLogVo careReFundLogVo);
}
