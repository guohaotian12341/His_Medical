package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.SchedulingVo;
import com.ruoyi.edu.mapper.SchedulingMapper;
import com.ruoyi.edu.service.SchedulingIService;
import com.ruoyi.edu.util.RandomNumberUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: SchedulingServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/29 17:39
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Service
@DataSource(DataSourceType.SLAVE)
public class SchedulingServiceImpl extends ServiceImpl<SchedulingMapper, SchedulingVo> implements SchedulingIService {
    @Autowired
    private SchedulingMapper mapper;


    /**
     * @Author:guohaotian
     * @Description:根据时间、科室、日期查询排班、科室、医生、挂号费等信息
     * @Param:schedulingVo
     * @Return:java.util.List<com.ruoyi.edu.domain.SchedulingVo>
     * @Date:2022/6/30~10:10
     */
    @Override
    public List<SchedulingVo> querySchDuLing(SchedulingVo schedulingVo) {
        return mapper.querySchDuLing(schedulingVo);
    }


    /**
     * @Author:guohaotian
     * @Description:根据排班id去查询排班、医生、挂号费、科室等信息
     * @Param:schedulingVo
     * @Return:com.ruoyi.edu.domain.SchedulingVo
     * @Date:2022/6/30~10:11
     */
    @Override
    public SchedulingVo querySchDuLingById(SchedulingVo schedulingVo) {
        return mapper.querySchDuLingById(schedulingVo);
    }

    /**
     * @Author:guohaotian
     * @Description:挂号成功后修改号源
     * @Param:schedulingVo
     * @Return:java.lang.Integer
     * @Date:2022/7/1~8:28
     */
    @Override
    public Integer updateSchDuLingById(SchedulingVo schedulingVo) {
        return mapper.updateById(schedulingVo);
    }
}
