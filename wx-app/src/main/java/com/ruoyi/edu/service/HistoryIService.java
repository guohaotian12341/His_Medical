package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.HistoryVo;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HistoryIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/30 10:05
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface HistoryIService extends IService<HistoryVo> {
    Integer createHistory(HistoryVo historyVo);
    HistoryVo historyByRegistrationId(Integer registrationId);
}
