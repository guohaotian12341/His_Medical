package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.mapper.PatientMapper;
import com.ruoyi.edu.service.PatientIService;
import com.ruoyi.edu.domain.PatientVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: PatientServiceImp
 * @package_Name: com.sykj.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/18 9:02
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */


@Service
@DataSource(DataSourceType.SLAVE)
public class PatientServiceImp extends ServiceImpl<PatientMapper, PatientVo> implements PatientIService {
    @Autowired
    private PatientMapper mapper;
    @Override/**
     * @Author:guohaotian
     * @Description: 根据微信唯一标识查询患者信息
     * @Param:openId
     * @Return:java.util.List<com.ruoyi.edu.domain.Patient>
     * @Date:2022/6/27~8:41
     */
    public List<PatientVo> QueryByOpenId(String openId) {
        QueryWrapper<PatientVo> qw=new QueryWrapper<>();
        qw.eq("openid",openId);
        qw.eq("is_del",'0');
        List<PatientVo> patientVos = mapper.selectList(qw);
        return patientVos;
    }


    /**
     * @Author:guohaotian
     * @Description:根据患者id去查单个患者信息
     * @Param:patientId
     * @Return:com.ruoyi.edu.domain.PatientVo
     * @Date:2022/6/28~9:42
     */
    @Override
    public PatientVo findByPatientId(String patientId) {
        QueryWrapper<PatientVo> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("patient_id",patientId);
        return mapper.selectOne(queryWrapper);
    }

    /**
     * @Author:guohaotian
     * @Description:移除患者
     * @Param:patientVo
     * @Return:java.lang.Integer
     * @Date:2022/6/28~9:42
     */
    @Override
    public Integer DelPatient(PatientVo patientVo) {
        return mapper.delPatient(patientVo);
    }


    /**
     * @Author:guohaotian
     * @Description:添加患者
     * @Param:patientVo
     * @Return:java.lang.Integer
     * @Date:2022/6/28~9:43
     */
    @Override
    public Integer InsertPatient(PatientVo patientVo) {
        patientVo.setAllergyInfo("无");
        return mapper.insert(patientVo);
    }
}
