package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.DepartmentVo;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DepartmentIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/28 9:33
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface DepartmentIService extends IService<DepartmentVo> {
    public List<DepartmentVo> findAll();
}
