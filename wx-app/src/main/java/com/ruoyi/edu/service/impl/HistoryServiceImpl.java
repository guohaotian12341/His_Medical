package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.domain.HistoryVo;
import com.ruoyi.edu.mapper.HistoryMapper;
import com.ruoyi.edu.service.HistoryIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HistoryServiceImpl
 * @package_Name: com.ruoyi.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/30 10:06
 * @Description:历史病例
 * @To change this template use File | Settings | File Templates.
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class HistoryServiceImpl extends ServiceImpl<HistoryMapper, HistoryVo> implements HistoryIService {
   @Autowired
   private HistoryMapper mapper;

    /**
     * @Author:guohaotian
     * @Description:创建病例
     * @Param:historyVo
     * @Return:java.lang.Integer
     * @Date:2022/6/30~10:10
     */
    @Override
    public Integer createHistory(HistoryVo historyVo) {
        return mapper.insert(historyVo);
    }

    /**
     * @Author:guohaotian
     * @Description:根据挂号id去查找病例，查看医生给的诊断
     * @Param:registrationId
     * @Return:com.ruoyi.edu.domain.HistoryVo
     * @Date:2022/7/14~10:09
     */
    @Override
    public HistoryVo historyByRegistrationId(Integer registrationId) {
        QueryWrapper<HistoryVo> qr=new QueryWrapper<>();
        qr.eq("registration_id",registrationId);
        return mapper.selectOne(qr);
    }
}
