package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.CareHistoryVo;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareHistoryIService
 * @package_Name: com.ruoyi.edu.service
 * @User: guohaotian
 * @Date: 2022/6/30 15:49
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface CareHistoryIService extends IService<CareHistoryVo> {
    public Integer createCareHistory(CareHistoryVo historyVo);
}
