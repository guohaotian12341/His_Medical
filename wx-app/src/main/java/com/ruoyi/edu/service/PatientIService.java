package com.ruoyi.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.edu.domain.PatientVo;
import com.ruoyi.edu.util.RandomNumberUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: PatientIService
 * @package_Name: com.sykj.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/18 9:02
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

public interface PatientIService extends IService<PatientVo> {
    public List<PatientVo> QueryByOpenId(String openId);
    public PatientVo findByPatientId(String patientId);
    public Integer DelPatient(PatientVo patientVo);
    public Integer InsertPatient(PatientVo patientVo);
}
