package com.ruoyi.edu.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.edu.mapper.WxOpenIdMapper;
import com.ruoyi.edu.service.WxOpenIdIService;
import com.ruoyi.edu.domain.WxOpenIdVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxOpenIdServiceImp
 * @package_Name: com.sykj.edu.service.impl
 * @User: guohaotian
 * @Date: 2022/6/17 15:28
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */



@Service
@DataSource(DataSourceType.SLAVE)
public class WxOpenIdServiceImp extends ServiceImpl<WxOpenIdMapper, WxOpenIdVo> implements WxOpenIdIService {
    @Autowired
    private WxOpenIdMapper mapper;

}
