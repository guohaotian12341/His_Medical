package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.CareHistoryVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareHistoryMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/30 15:47
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface CareHistoryMapper extends BaseMapper<CareHistoryVo> {
}
