package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.DepartmentVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DepartmentMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/28 9:32
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<DepartmentVo> {
}
