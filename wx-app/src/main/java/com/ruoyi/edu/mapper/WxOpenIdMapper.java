package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.WxOpenIdVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxOpenIdMapper
 * @package_Name: com.sykj.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/17 15:32
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */
@Mapper
public interface WxOpenIdMapper extends BaseMapper<WxOpenIdVo> {
}
