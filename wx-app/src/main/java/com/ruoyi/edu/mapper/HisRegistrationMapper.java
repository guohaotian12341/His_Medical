package com.ruoyi.edu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.edu.domain.HisRegistrationVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HisRegistrationMapper
 * @package_Name: com.ruoyi.edu.mapper
 * @User: guohaotian
 * @Date: 2022/6/30 14:44
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@Mapper
public interface HisRegistrationMapper extends BaseMapper<HisRegistrationVo> {
    List<HisRegistrationVo> queryHisRegistrationByPatientId(HisRegistrationVo hisRegistrationVo);
    List<HisRegistrationVo> queryHisRegistrationStatus(HisRegistrationVo hisRegistrationVo);
}
