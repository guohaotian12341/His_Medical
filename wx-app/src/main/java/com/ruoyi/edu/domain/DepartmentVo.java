package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DepartmentVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/28 9:32
 * @Description:科室表
 * @To change this template use File | Settings | File Templates.
 */

@TableName("dzm_his_department")
@Data
@Alias("department")
public class DepartmentVo {
    @TableId(type = IdType.AUTO)
    private Integer did;//	bigint(20) unsigned		NO	是	科室id
    private Integer createTime;//	int(10) unsigned	0	NO		创建时间
    private Integer updateTime;//	int(10) unsigned	0	NO		编辑时间
    private String departmentName;//	varchar(50)		NO		科室名称
    private String departmentNumber;//	varchar(50)		NO		科室编号
    private Integer hid;//	int(10)		NO		医院id

    public DepartmentVo(Integer did, Integer createTime, Integer updateTime, String departmentName, String departmentNumber, Integer hid) {
        this.did = did;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.departmentName = departmentName;
        this.departmentNumber = departmentNumber;
        this.hid = hid;
    }

    public DepartmentVo() {
    }
}
