package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: RegisteredFeeSubVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/29 17:07
 * @Description:挂号子费用表
 * @To change this template use File | Settings | File Templates.
 */

@TableName("dzm_his_registeredfee_sub")
@Data
public class RegisteredFeeSubVo {
    @TableId(type = IdType.AUTO)
    private Integer regSubId;//	int(10) unsigned		NO	是
    private Integer regId;//	int(10) unsigned		NO		挂号费用ID
    private String subRegisteredfeeName;//	varchar(255)		NO		挂号费用子名称
    private Double subRegisteredfeeFee;//	decimal(8,2)		NO		子费用

    public RegisteredFeeSubVo(Integer regSubId, Integer regId, String subRegisteredfeeName, Double subRegisteredfeeFee) {
        this.regSubId = regSubId;
        this.regId = regId;
        this.subRegisteredfeeName = subRegisteredfeeName;
        this.subRegisteredfeeFee = subRegisteredfeeFee;
    }

    public RegisteredFeeSubVo() {
    }
}
