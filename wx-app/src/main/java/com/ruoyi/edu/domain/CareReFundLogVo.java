package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CareReFundLogVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/7/1 16:31
 * @Description:退费记录
 * @To change this template use File | Settings | File Templates.
 */

@TableName("dzm_his_care_refundlog")
@Data
@Alias("careReFunLog")
public class CareReFundLogVo {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer logId;//	int(10) unsigned		NO	是
    private Integer pkgId;//	int(10) unsigned		YES		care_pkg.id
    private Integer orderId;//	int(10) unsigned	0	YES		his_care_order.id
    private String platformCode;//	varchar(128)		YES		支付平台交易单号
    private Integer paymentPlatform;//	smallint(5) unsigned	0	YES		支付方式：0现金，1微信，2支付宝，3，4，5....
    private Double refundAmount;//	decimal(10,2)	0.00	YES		支付金额
    private Integer status;//	tinyint(1) unsigned	0	YES		状态，0失败，1成功
    private String addtime;//	timestamp	CURRENT_TIMESTAMP	YES		添加时间
    private String admUid;//	int(10) unsigned	0	YES		处理人id
    private String admIp;//	varchar(32)		YES		处理人ip
    private String admMemo;//	varchar(255)		YES		备注

    public CareReFundLogVo(Integer logId, Integer pkgId, Integer orderId, String platformCode, Integer paymentPlatform, Double refundAmount, Integer status, String addtime, String admUid, String admIp, String admMemo) {
        this.logId = logId;
        this.pkgId = pkgId;
        this.orderId = orderId;
        this.platformCode = platformCode;
        this.paymentPlatform = paymentPlatform;
        this.refundAmount = refundAmount;
        this.status = status;
        this.addtime = addtime;
        this.admUid = admUid;
        this.admIp = admIp;
        this.admMemo = admMemo;
    }

    public CareReFundLogVo() {
    }
}
