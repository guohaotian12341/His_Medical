package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxOpenIdVo
 * @package_Name: com.sykj.edu.vo
 * @User: guohaotian
 * @Date: 2022/6/17 15:06
 * @Description:微信信息（openid）和用户id的关系表
 * @To change this template use File | Settings | File Templates.
 */

@Data
@TableName("dzm_his_wxopenid")
public class WxOpenIdVo{
    private Integer id;//	int(10) unsigned		NO	是
    private String appid;//	varchar(20)		YES		appid预留分表用
    private String openid;//	varchar(32)		YES		微信openid
    private Integer userid;//	int(10) unsigned		YES		用户id
    private String usertype;//	tinyint(1) unsigned	0	YES		用户类型，0系统管理员，1诊所医院，2医生，3患者
    private String addtime;//	timestamp	CURRENT_TIMESTAMP	YES		添加时间
    private Integer patient_id;//int				患者id

    public WxOpenIdVo(Integer id, String appid, String openid, Integer userid, String usertype, String addtime, Integer patient_id) {
        this.id = id;
        this.appid = appid;
        this.openid = openid;
        this.userid = userid;
        this.usertype = usertype;
        this.addtime = addtime;
        this.patient_id = patient_id;
    }

    public WxOpenIdVo() {
    }
}
