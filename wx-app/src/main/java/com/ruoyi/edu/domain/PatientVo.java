package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: Patient
 * @package_Name: com.sykj.edu.vo
 * @User: guohaotian
 * @Date: 2022/6/18 8:51
 * @Description:患者用户表
 * @To change this template use File | Settings | File Templates.
 */

@Data
@TableName("dzm_patient")
@Alias("patient")
public class PatientVo {
    @TableId(value = "patient_Id")
    private Integer patientId;//	int(11)		NO	是	主键
    private Integer hospitalId;//	int(10)	0	YES		所属医院、诊所
    private String name;//	varchar(50)		NO		患者姓名
    private String openid;//	varchar(80)	0	YES		微信openid
    private String mobile;//	varchar(11)		NO		患者电话
    private Long updateTime;//	int(10)		NO		修改时间
    private String password;//	varchar(60)		YES		登录密码
    private Integer sex;//	tinyint(2)	0	YES		患者性别1男2女
    private String birthday;//	varchar(50)		YES		生日
    private String idCard;//	char(18)		YES		身份证
    private Integer isFinal;//	tinyint(4)	0	YES		是否完善信息，0否1已完善
    private Integer lastLoginIp;//	int(10)	0	YES		最后登录ip
    private Integer lastLoginTime;//	int(10)	0	YES		最后登录时间
    private String address;//	varchar(120)		YES		地址信息
    private Long createTime;//	int(10)	0	NO		注册时间
    private String provinceId;//	int(11)	0	YES		省区id
    private String cityId;//	int(11)	0	YES		市区id
    private String districtId;//	int(11)	0	YES		县区id
    private String allergyInfo;//	varchar(100)		YES		过敏信息
    private Integer isDel;//	tinyint(1)	0	YES		是否移除 0：正常 1：删除

    public PatientVo(Integer patientId, Integer hospitalId, String name, String openid, String mobile, Long updateTime, String password, Integer sex, String birthday, String idCard, Integer isFinal, Integer lastLoginIp, Integer lastLoginTime, String address, Long createTime, String provinceId, String cityId, String districtId, String allergyInfo, Integer isDel) {
        this.patientId = patientId;
        this.hospitalId = hospitalId;
        this.name = name;
        this.openid = openid;
        this.mobile = mobile;
        this.updateTime = updateTime;
        this.password = password;
        this.sex = sex;
        this.birthday = birthday;
        this.idCard = idCard;
        this.isFinal = isFinal;
        this.lastLoginIp = lastLoginIp;
        this.lastLoginTime = lastLoginTime;
        this.address = address;
        this.createTime = createTime;
        this.provinceId = provinceId;
        this.cityId = cityId;
        this.districtId = districtId;
        this.allergyInfo = allergyInfo;
        this.isDel = isDel;
    }


    public PatientVo() {
    }
}
