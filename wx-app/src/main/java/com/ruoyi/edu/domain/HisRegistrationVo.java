package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: HisRegistrationVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/30 10:34
 * @Description:门诊挂号
 * @To change this template use File | Settings | File Templates.
 */

@Data
@TableName("dzm_his_registration")
@Alias("registration")
public class HisRegistrationVo {
    @TableId(type = IdType.AUTO)
    private Integer registrationId;//	bigint(20) unsigned		NO	是
    private Integer patientId;//	int(10)		NO		患者ID
    private Integer physicianId;//	int(10)		NO		医生ID
    private Integer operatorId;//	int(10)		NO		操作员ID
    private Integer companyId;//	int(10)		NO		诊所ID
    private Integer departmentId;//	int(10)		NO		科室ID
    private Integer registeredfeeId;//	int(10)		NO		挂号费用ID
    private Float registrationAmount;//	float(8,2)		NO		挂号总金额
    @TableField(fill = FieldFill.INSERT)
    private Long registrationNumber;//	bigint(20)		NO		挂号编号
    private Integer registrationStatus;//	tinyint(2)	1	NO		挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5,为未付款,6，为部分支付
    private Integer schedulingId;//	int(10)		NO		排班主表ID
    @TableField(fill = FieldFill.INSERT)
    private Long createTime;//	int(11)		NO		创建时间
    @TableField(fill=FieldFill.INSERT_UPDATE)
    private Long updateTime;//	int(11)		NO		更新时间
    private Integer pkgId;//	int(10) unsigned	0	YES		收费总表care_pkg.id
    @TableField(exist = false)
    private PatientVo patientVo;//患者信息
    @TableField(exist = false)
    private DoctorVo doctorVo;//医生信息
    @TableField(exist = false)
    private DepartmentVo departmentVo;//科室信息

    public HisRegistrationVo(Integer registrationId, Integer patientId, Integer physicianId, Integer operatorId, Integer companyId, Integer departmentId, Integer registeredfeeId, Float registrationAmount, Long registrationNumber, Integer registrationStatus, Integer schedulingId, Long createTime, Long updateTime, Integer pkgId, PatientVo patientVo, DoctorVo doctorVo, DepartmentVo departmentVo) {
        this.registrationId = registrationId;
        this.patientId = patientId;
        this.physicianId = physicianId;
        this.operatorId = operatorId;
        this.companyId = companyId;
        this.departmentId = departmentId;
        this.registeredfeeId = registeredfeeId;
        this.registrationAmount = registrationAmount;
        this.registrationNumber = registrationNumber;
        this.registrationStatus = registrationStatus;
        this.schedulingId = schedulingId;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.pkgId = pkgId;
        this.patientVo = patientVo;
        this.doctorVo = doctorVo;
        this.departmentVo = departmentVo;
    }

    public HisRegistrationVo(Integer registrationId, Integer pkgId) {
        this.registrationId = registrationId;
        this.pkgId = pkgId;
    }

    public HisRegistrationVo() {
    }
}
