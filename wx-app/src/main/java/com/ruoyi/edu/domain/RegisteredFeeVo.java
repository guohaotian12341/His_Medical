package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: RegisteredfeeVo
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/29 17:04
 * @Description:挂号费用
 * @To change this template use File | Settings | File Templates.
 */
@TableName("dzm_his_registeredfee")
@Data
@Alias("registeredFee")
public class RegisteredFeeVo {
    private Integer regId;//	int(11) unsigned		NO	是
    private Integer mid;//	int(11)		NO		用户id
    private Integer companyId;//	int(11)		NO		公司ID
    private String registeredfeeName;//	varchar(255)		NO		挂号费用名称
    private Double registeredfeeFee;//	decimal(8,2) unsigned		NO		金额
    private Double registeredfeeSubFee;//	decimal(8,2) unsigned		NO		子费用总数
    private Double registeredfeeAggregateAmount;//	decimal(8,2) unsigned		NO		挂号费用总金额
    private Integer numberOfSub;//	int(5)		NO		子费用数量
    private Integer createTime;//	int(10)		NO		创建时间

    public RegisteredFeeVo(Integer regId, Integer mid, Integer companyId, String registeredfeeName, Double registeredfeeFee, Double registeredfeeSubFee, Double registeredfeeAggregateAmount, Integer numberOfSub, Integer createTime) {
        this.regId = regId;
        this.mid = mid;
        this.companyId = companyId;
        this.registeredfeeName = registeredfeeName;
        this.registeredfeeFee = registeredfeeFee;
        this.registeredfeeSubFee = registeredfeeSubFee;
        this.registeredfeeAggregateAmount = registeredfeeAggregateAmount;
        this.numberOfSub = numberOfSub;
        this.createTime = createTime;
    }

    public RegisteredFeeVo() {
    }
}
