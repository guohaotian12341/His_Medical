package com.ruoyi.edu.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import org.apache.ibatis.type.Alias;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePkg
 * @package_Name: com.ruoyi.edu.domain
 * @User: guohaotian
 * @Date: 2022/6/30 15:54
 * @Description:收费总表
 * @To change this template use File | Settings | File Templates.
 */
@TableName("dzm_his_care_pkg")
@Data
@Alias("carePkg")
public class CarePkgVo {
    @TableId(type = IdType.AUTO)
    private Integer id;//	int(10) unsigned		NO	是
    private Integer hospitalId;//	int(10) unsigned	0	YES		医院ID
    private Integer doctorId;//	int(10) unsigned	0	YES		医生ID
    private Integer patientId;//	int(10) unsigned	0	YES		患者ID
    private Integer careHistoryId;//	int(10) unsigned	0	YES		历史病例ID
    private Integer registrationId;//	int(10) unsigned	0	YES		挂号ID
    private String orderCode;//	varchar(64)		YES		商户订单号
    private Double amount;//	decimal(10,2)	0.00	YES		应付金额
    private Double olPayPart;//	decimal(10,2)	0.00	YES		在线支付部分
    private Integer typeId;//	tinyint(1) unsigned	0	YES		收费类型：0就诊处，1挂号处，2问答，3...
    private Integer status;//	tinyint(1) unsigned	0	YES		状态:0未支付，1已支付，2确认收款，3申请退款，4已退款,5部分支付,6完成交易（如：已发药），7部分退款
    @TableField(fill= FieldFill.INSERT)
    private Long addtime;//	int(10) unsigned	0	YES		插入时间
    private Integer opPlace;//	tinyint(1) unsigned	0	YES		操作地点：1售药，2查检项目，3附加费用，4挂号
    @TableField(exist = false)
    private HisRegistrationVo hisRegistrationVo;//挂号

    public CarePkgVo(Integer id, Integer hospitalId, Integer doctorId, Integer patientId, Integer careHistoryId, Integer registrationId, String orderCode, Double amount, Double olPayPart, Integer typeId, Integer status, Long addtime, Integer opPlace, HisRegistrationVo hisRegistrationVo) {
        this.id = id;
        this.hospitalId = hospitalId;
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.careHistoryId = careHistoryId;
        this.registrationId = registrationId;
        this.orderCode = orderCode;
        this.amount = amount;
        this.olPayPart = olPayPart;
        this.typeId = typeId;
        this.status = status;
        this.addtime = addtime;
        this.opPlace = opPlace;
        this.hisRegistrationVo = hisRegistrationVo;
    }

    public CarePkgVo() {
    }
}
