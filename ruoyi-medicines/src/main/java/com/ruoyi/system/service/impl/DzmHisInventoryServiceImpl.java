package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisInventory;
import com.ruoyi.system.domain.Shenhe;
import com.ruoyi.system.mapper.DzmHisInventoryMapper;
import com.ruoyi.system.service.IInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisInventoryServiceImpl implements IInventoryService {
    @Autowired
    private DzmHisInventoryMapper mapper;

    @Override
    public List<DzmHisInventory> All(DzmHisInventory inventory) {
        return mapper.All(inventory);
    }

    @Override
    public Integer del(Integer inventoryId) {
        return mapper.del(inventoryId);
    }

    @Override
    public List<Shenhe> Allss(Shenhe batches) {
        return mapper.ALLss(batches);
    }

    @Override
    public Integer delss(Integer batchesOfInventoryId) {
        return mapper.delss(batchesOfInventoryId);
    }

    @Override
    public Integer update(Shenhe shenhe) {
        return mapper.update(shenhe);
    }

    @Override
    public Integer updates(Shenhe shenhe) {
        return mapper.updates(shenhe);
    }
}
