package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.DzmHisSupplier;
import com.ruoyi.system.mapper.DzmHisSupplierMapper;
import com.ruoyi.system.service.IDzmHisSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 供应商Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisSupplierServiceImpl implements IDzmHisSupplierService 
{
    @Autowired
    private DzmHisSupplierMapper dzmHisSupplierMapper;

    /**
     * 查询供应商
     * 
     * @param sid 供应商主键
     * @return 供应商
     */
    @Override
    public DzmHisSupplier selectDzmHisSupplierBySid(String sid)
    {
        return dzmHisSupplierMapper.selectDzmHisSupplierBySid(sid);
    }

    /**
     * 查询供应商列表
     * 
     * @param dzmHisSupplier 供应商
     * @return 供应商
     */
    @Override
    public List<DzmHisSupplier> selectDzmHisSupplierList(DzmHisSupplier dzmHisSupplier)
    {
        return dzmHisSupplierMapper.selectDzmHisSupplierList(dzmHisSupplier);
    }

    /**
     * 新增供应商
     * 
     * @param dzmHisSupplier 供应商
     * @return 结果
     */
    @Override
    public int insertDzmHisSupplier(DzmHisSupplier dzmHisSupplier)
    {
        dzmHisSupplier.setCreateTime(DateUtils.getNowDate());
        return dzmHisSupplierMapper.insertDzmHisSupplier(dzmHisSupplier);
    }

    /**
     * 修改供应商
     * 
     * @param dzmHisSupplier 供应商
     * @return 结果
     */
    @Override
    public int updateDzmHisSupplier(DzmHisSupplier dzmHisSupplier)
    {
        dzmHisSupplier.setUpdateTime(DateUtils.getNowDate());
        return dzmHisSupplierMapper.updateDzmHisSupplier(dzmHisSupplier);
    }

    /**
     * 批量删除供应商
     * 
     * @param sids 需要删除的供应商主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisSupplierBySids(String[] sids)
    {
        return dzmHisSupplierMapper.deleteDzmHisSupplierBySids(sids);
    }

    /**
     * 删除供应商信息
     * 
     * @param sid 供应商主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisSupplierBySid(String sid)
    {
        return dzmHisSupplierMapper.deleteDzmHisSupplierBySid(sid);
    }
}
