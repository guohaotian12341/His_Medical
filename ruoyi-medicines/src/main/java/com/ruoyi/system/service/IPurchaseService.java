package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.DzmHisSupplier;
import com.ruoyi.system.domain.PurchaseInfo;

import java.util.List;

public interface IPurchaseService extends IService<PurchaseInfo> {


    List<DzmHisMedicines> queryMedicines();

    Integer addpurchase(PurchaseInfo purchaseInfo);

    List<DzmHisSupplier> getSupplier();

    String createID();

    List<DzmHisMedicines> querylist(String[] arrs);

    Integer addBatchesInventory(BatchesOfInventoryInfo batches);
}
