package com.ruoyi.system.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.DzmHisPurchase;
import com.ruoyi.system.mapper.DzmHisPurchaseMapper;
import com.ruoyi.system.service.IDzmHisPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 采购信息Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-27
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisPurchaseServiceImpl implements IDzmHisPurchaseService 
{
    @Autowired
    private DzmHisPurchaseMapper dzmHisPurchaseMapper;

    /**
     * 查询采购信息
     * 
     * @param purchaseId 采购信息主键
     * @return 采购信息
     */
    @Override
    public DzmHisPurchase selectDzmHisPurchaseByPurchaseId(String purchaseId)
    {
        return dzmHisPurchaseMapper.selectDzmHisPurchaseByPurchaseId(purchaseId);
    }

    /**
     * 查询采购信息列表
     * 
     * @param dzmHisPurchase 采购信息
     * @return 采购信息
     */
    @Override
    public List<DzmHisPurchase> selectDzmHisPurchaseList(DzmHisPurchase dzmHisPurchase)
    {
        return dzmHisPurchaseMapper.selectDzmHisPurchaseList(dzmHisPurchase);
    }

    /**
     * 新增采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    @Transactional
    @Override
    public int insertDzmHisPurchase(DzmHisPurchase dzmHisPurchase)
    {
        dzmHisPurchase.setCreateTime(DateUtils.getNowDate());
        int rows = dzmHisPurchaseMapper.insertDzmHisPurchase(dzmHisPurchase);
        insertDzmHisMedicines(dzmHisPurchase);
        return rows;
    }

    /**
     * 修改采购信息
     * 
     * @param dzmHisPurchase 采购信息
     * @return 结果
     */
    @Transactional
    @Override
    public int updateDzmHisPurchase(DzmHisPurchase dzmHisPurchase)
    {
        dzmHisPurchaseMapper.deleteDzmHisMedicinesByMedicinesNumber(dzmHisPurchase.getPurchaseId());
        insertDzmHisMedicines(dzmHisPurchase);
        return dzmHisPurchaseMapper.updateDzmHisPurchase(dzmHisPurchase);
    }

    /**
     * 批量删除采购信息
     * 
     * @param purchaseIds 需要删除的采购信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDzmHisPurchaseByPurchaseIds(String[] purchaseIds)
    {
        dzmHisPurchaseMapper.deleteDzmHisMedicinesByMedicinesNumbers(purchaseIds);
        return dzmHisPurchaseMapper.deleteDzmHisPurchaseByPurchaseIds(purchaseIds);
    }

    /**
     * 删除采购信息信息
     * 
     * @param purchaseId 采购信息主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteDzmHisPurchaseByPurchaseId(String purchaseId)
    {
        dzmHisPurchaseMapper.deleteDzmHisMedicinesByMedicinesNumber(purchaseId);
        return dzmHisPurchaseMapper.deleteDzmHisPurchaseByPurchaseId(purchaseId);
    }

    /**
     * 新增药品信息信息
     * 
     * @param dzmHisPurchase 采购信息对象
     */
    public void insertDzmHisMedicines(DzmHisPurchase dzmHisPurchase)
    {
        List<DzmHisMedicines> dzmHisMedicinesList = dzmHisPurchase.getDzmHisMedicinesList();
        String purchaseId = dzmHisPurchase.getPurchaseId();
        if (StringUtils.isNotNull(dzmHisMedicinesList))
        {
            List<DzmHisMedicines> list = new ArrayList<DzmHisMedicines>();
            for (DzmHisMedicines dzmHisMedicines : dzmHisMedicinesList)
            {
                dzmHisMedicines.setMedicinesNumber(purchaseId);
                list.add(dzmHisMedicines);
            }
            if (list.size() > 0)
            {
                dzmHisPurchaseMapper.batchDzmHisMedicines(list);
            }
        }
    }
}
