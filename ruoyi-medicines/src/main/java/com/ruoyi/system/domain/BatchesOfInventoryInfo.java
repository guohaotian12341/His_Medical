package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 批次库存表
 */
@Data
@TableName("dzm_his_batches_of_inventory")
public class BatchesOfInventoryInfo extends BaseEntity {

    @TableId(type = IdType.AUTO)
    private Integer batchesOfInventoryId;// int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '批次库存ID',
    private Integer companyId;// int(10) NOT NULL COMMENT '诊所ID',
    private Integer supplierId;// int(10) NOT NULL COMMENT '供应商ID',
    private String batchesOfInventoryNumber;// bigint(20) NOT NULL COMMENT '采购单编号',
    private String  purchasingAgentId;// varchar(50) NOT NULL COMMENT '采购员ID',
    private Double batchesOfInventoryTotalMoney;// decimal(10,2) NOT NULL COMMENT '采购总金额',
    private String  batchesOfInventoryDate;// varchar(20) NOT NULL COMMENT '制单日期',
    private Integer  batchesOfInventoryStatus;// int(3) NOT NULL DEFAULT '1' COMMENT '审核标记；未审核：1，已审核：2',
    private String  batchesOfInventoryVerifier;// varchar(50) DEFAULT NULL COMMENT '审核人员ID',
    private String  batchesOfInventoryVerifierDate;// varchar(20) DEFAULT NULL COMMENT '审核日期',
    private Date  createTime;// int(11) NOT NULL COMMENT '创建时间',
    private Date updateTime;// int(11) DEFAULT NULL COMMENT '更新时间',

    private String supplierName;//供应商
}
