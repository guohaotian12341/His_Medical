package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 采购信息表
 */
@Data
@TableName("dzm_his_purchase")
public class PurchaseInfo extends BaseEntity {
   @TableId(type = IdType.AUTO)
    private Integer purchaseId;//` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '采购信息ID',
    private Integer medicinesId;//` int(10) NOT NULL COMMENT '医院药品关联表：hmr_id',
    private String batchesOfInventoryId;//` int(10) NOT NULL COMMENT '批次库存ID',
    private Integer purchaseNum;//` int(10) NOT NULL COMMENT '采购数量',
    private String  purchaseUnit;//` varchar(50) NOT NULL COMMENT '采购单位',
    private Double purchaseTradePrice;//` decimal(10,2) NOT NULL COMMENT '批发价',
    private Double purchasePrescriptionPrice;//` decimal(10,2) NOT NULL COMMENT '处方价',
    private Double purchaseTradeTotalAmount;//` decimal(10,2) NOT NULL COMMENT '采购批发总额',
    private Double purchasePrescriptionTotalAmount;//` decimal(10,2) NOT NULL COMMENT '采购处方总额',
    private Date createTime;//` int(11) NOT NULL COMMENT '创建时间',
    private Integer hmrId;//` int(10) unsigned DEFAULT '0',

   private String   medicinesName ;//药品名称
   private String    producter ;// ` varchar(50) DEFAULT '' COMMENT '生产厂家',

}
