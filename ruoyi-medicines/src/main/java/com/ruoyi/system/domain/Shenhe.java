package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 批次库存表
 */

public class Shenhe extends BaseEntity {

    private static final long serialVersionUID = 1L;
    private Integer batchesOfInventoryId;// int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '批次库存ID',
    private Integer companyId;// int(10) NOT NULL COMMENT '诊所ID',
    private Integer supplierId;// int(10) NOT NULL COMMENT '供应商ID',
    private String batchesOfInventoryNumber;// bigint(20) NOT NULL COMMENT '采购单编号',
    private String  purchasingAgentId;// varchar(50) NOT NULL COMMENT '采购员ID',
    private Double batchesOfInventoryTotalMoney;// decimal(10,2) NOT NULL COMMENT '采购总金额',
    private String  batchesOfInventoryDate;// varchar(20) NOT NULL COMMENT '制单日期',
    private Integer  batchesOfInventoryStatus;// int(3) NOT NULL DEFAULT '1' COMMENT '审核标记；未审核：1，已审核：2',
    private String  batchesOfInventoryVerifier;// varchar(50) DEFAULT NULL COMMENT '审核人员ID',
    private String  batchesOfInventoryVerifierDate;// varchar(20) DEFAULT NULL COMMENT '审核日期',
    private Date  createTime;// int(11) NOT NULL COMMENT '创建时间',
    private Date updateTime;// int(11) DEFAULT NULL COMMENT '更新时间',

    @Override
    public String toString() {
        return "Shenhe{" +
                "batchesOfInventoryId=" + batchesOfInventoryId +
                ", companyId=" + companyId +
                ", supplierId=" + supplierId +
                ", batchesOfInventoryNumber='" + batchesOfInventoryNumber + '\'' +
                ", purchasingAgentId='" + purchasingAgentId + '\'' +
                ", batchesOfInventoryTotalMoney=" + batchesOfInventoryTotalMoney +
                ", batchesOfInventoryDate='" + batchesOfInventoryDate + '\'' +
                ", batchesOfInventoryStatus=" + batchesOfInventoryStatus +
                ", batchesOfInventoryVerifier='" + batchesOfInventoryVerifier + '\'' +
                ", batchesOfInventoryVerifierDate='" + batchesOfInventoryVerifierDate + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }

    public Integer getBatchesOfInventoryId() {
        return batchesOfInventoryId;
    }

    public void setBatchesOfInventoryId(Integer batchesOfInventoryId) {
        this.batchesOfInventoryId = batchesOfInventoryId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public String getBatchesOfInventoryNumber() {
        return batchesOfInventoryNumber;
    }

    public void setBatchesOfInventoryNumber(String batchesOfInventoryNumber) {
        this.batchesOfInventoryNumber = batchesOfInventoryNumber;
    }

    public String getPurchasingAgentId() {
        return purchasingAgentId;
    }

    public void setPurchasingAgentId(String purchasingAgentId) {
        this.purchasingAgentId = purchasingAgentId;
    }

    public Double getBatchesOfInventoryTotalMoney() {
        return batchesOfInventoryTotalMoney;
    }

    public void setBatchesOfInventoryTotalMoney(Double batchesOfInventoryTotalMoney) {
        this.batchesOfInventoryTotalMoney = batchesOfInventoryTotalMoney;
    }

    public String getBatchesOfInventoryDate() {
        return batchesOfInventoryDate;
    }

    public void setBatchesOfInventoryDate(String batchesOfInventoryDate) {
        this.batchesOfInventoryDate = batchesOfInventoryDate;
    }

    public Integer getBatchesOfInventoryStatus() {
        return batchesOfInventoryStatus;
    }

    public void setBatchesOfInventoryStatus(Integer batchesOfInventoryStatus) {
        this.batchesOfInventoryStatus = batchesOfInventoryStatus;
    }

    public String getBatchesOfInventoryVerifier() {
        return batchesOfInventoryVerifier;
    }

    public void setBatchesOfInventoryVerifier(String batchesOfInventoryVerifier) {
        this.batchesOfInventoryVerifier = batchesOfInventoryVerifier;
    }

    public String getBatchesOfInventoryVerifierDate() {
        return batchesOfInventoryVerifierDate;
    }

    public void setBatchesOfInventoryVerifierDate(String batchesOfInventoryVerifierDate) {
        this.batchesOfInventoryVerifierDate = batchesOfInventoryVerifierDate;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
