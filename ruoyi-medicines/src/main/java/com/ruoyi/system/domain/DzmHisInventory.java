package com.ruoyi.system.domain;

import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

public class DzmHisInventory extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private Integer  inventoryId;//` int unsigned NOT NULL AUTO_INCREMENT COMMENT '库存ID',
    private Integer   hmrId;//` int NOT NULL COMMENT '药品ID',
    private Integer         companyId;//` int NOT NULL COMMENT '诊所ID',
    private Integer         inventoryNum;//` bigint NOT NULL COMMENT '库存数量',
    private String       inventoryUnit;//` varchar(50) NOT NULL COMMENT '单位',
    private Integer         inventoryTradePrice;//` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '批发价',
    private Integer        inventoryPrescriptionPrice;//` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '处方价',
    private Integer        inventoryTradeTotalAmount;//` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '批发额',
    private Integer         inventoryPrescriptionTotalAmount;//` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '处方额',
    private String        earlyWarning;//` varchar(50) NOT NULL DEFAULT '0' COMMENT '库存预警',
    private Date updateTime;//` int NOT NULL COMMENT '更新时间',
    private String medicinesName;//

    @Override
    public String toString() {
        return "DzmHisInventory{" +
                "inventoryId=" + inventoryId +
                ", hmrId=" + hmrId +
                ", companyId=" + companyId +
                ", inventoryNum=" + inventoryNum +
                ", inventoryUnit='" + inventoryUnit + '\'' +
                ", inventoryTradePrice=" + inventoryTradePrice +
                ", inventoryPrescriptionPrice=" + inventoryPrescriptionPrice +
                ", inventoryTradeTotalAmount=" + inventoryTradeTotalAmount +
                ", inventoryPrescriptionTotalAmount=" + inventoryPrescriptionTotalAmount +
                ", earlyWarning='" + earlyWarning + '\'' +
                ", updateTime=" + updateTime +
                ", medicinesName='" + medicinesName + '\'' +
                '}';
    }

    public String getMedicinesName() {
        return medicinesName;
    }

    public void setMedicinesName(String medicinesName) {
        this.medicinesName = medicinesName;
    }

    public Integer getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(Integer inventoryId) {
        this.inventoryId = inventoryId;
    }

    public Integer getHmrId() {
        return hmrId;
    }

    public void setHmrId(Integer hmrId) {
        this.hmrId = hmrId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getInventoryNum() {
        return inventoryNum;
    }

    public void setInventoryNum(Integer inventoryNum) {
        this.inventoryNum = inventoryNum;
    }

    public String getInventoryUnit() {
        return inventoryUnit;
    }

    public void setInventoryUnit(String inventoryUnit) {
        this.inventoryUnit = inventoryUnit;
    }

    public Integer getInventoryTradePrice() {
        return inventoryTradePrice;
    }

    public void setInventoryTradePrice(Integer inventoryTradePrice) {
        this.inventoryTradePrice = inventoryTradePrice;
    }

    public Integer getInventoryPrescriptionPrice() {
        return inventoryPrescriptionPrice;
    }

    public void setInventoryPrescriptionPrice(Integer inventoryPrescriptionPrice) {
        this.inventoryPrescriptionPrice = inventoryPrescriptionPrice;
    }

    public Integer getInventoryTradeTotalAmount() {
        return inventoryTradeTotalAmount;
    }

    public void setInventoryTradeTotalAmount(Integer inventoryTradeTotalAmount) {
        this.inventoryTradeTotalAmount = inventoryTradeTotalAmount;
    }

    public Integer getInventoryPrescriptionTotalAmount() {
        return inventoryPrescriptionTotalAmount;
    }

    public void setInventoryPrescriptionTotalAmount(Integer inventoryPrescriptionTotalAmount) {
        this.inventoryPrescriptionTotalAmount = inventoryPrescriptionTotalAmount;
    }

    public String getEarlyWarning() {
        return earlyWarning;
    }

    public void setEarlyWarning(String earlyWarning) {
        this.earlyWarning = earlyWarning;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
