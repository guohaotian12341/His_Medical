package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 药品信息对象 dzm_his_medicines
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
public class DzmHisMedicines extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String medicinesId;

    /** 药品编号 */
    @Excel(name = "药品编号")
    private String medicinesNumber;

    /** 药品名称 */
    @Excel(name = "药品名称")
    private String medicinesName;

    /** 药品分类  */
    @Excel(name = "药品分类 ")
    private String medicinesClass;

    /** 处方类型 */
    @Excel(name = "处方类型")
    private String prescriptionType;

    /** 单位（g/条） */
    @Excel(name = "单位", readConverterExp = "g=/条")
    private String unit;

    /** 换算量 */
    @Excel(name = "换算量")
    private Long conversion;

    /** 关键字 */
    @Excel(name = "关键字")
    private String keywords;

    /** 生产厂家 */
    @Excel(name = "生产厂家")
    private String producter;

    @Excel(name = "创建时间")
    private Date createTime;

    @Excel(name = "修改时间")
    private Date updateTime;


    /** 角色 */
    @Excel(name = "角色")
    private String mrole;

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setMedicinesId(String medicinesId)
    {
        this.medicinesId = medicinesId;
    }

    public String getMedicinesId()
    {
        return medicinesId;
    }
    public void setMedicinesNumber(String medicinesNumber)
    {
        this.medicinesNumber = medicinesNumber;
    }

    public String getMedicinesNumber()
    {
        return medicinesNumber;
    }
    public void setMedicinesName(String medicinesName)
    {
        this.medicinesName = medicinesName;
    }

    public String getMedicinesName()
    {
        return medicinesName;
    }
    public void setMedicinesClass(String medicinesClass)
    {
        this.medicinesClass = medicinesClass;
    }

    public String getMedicinesClass()
    {
        return medicinesClass;
    }
    public void setPrescriptionType(String prescriptionType)
    {
        this.prescriptionType = prescriptionType;
    }

    public String getPrescriptionType()
    {
        return prescriptionType;
    }
    public void setUnit(String unit)
    {
        this.unit = unit;
    }

    public String getUnit()
    {
        return unit;
    }
    public void setConversion(Long conversion)
    {
        this.conversion = conversion;
    }

    public Long getConversion()
    {
        return conversion;
    }
    public void setKeywords(String keywords)
    {
        this.keywords = keywords;
    }

    public String getKeywords()
    {
        return keywords;
    }
    public void setProducter(String producter)
    {
        this.producter = producter;
    }

    public String getProducter()
    {
        return producter;
    }
    public void setMrole(String mrole)
    {
        this.mrole = mrole;
    }

    public String getMrole()
    {
        return mrole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("medicinesId", getMedicinesId())
            .append("medicinesNumber", getMedicinesNumber())
            .append("medicinesName", getMedicinesName())
            .append("medicinesClass", getMedicinesClass())
            .append("prescriptionType", getPrescriptionType())
            .append("unit", getUnit())
            .append("conversion", getConversion())
            .append("keywords", getKeywords())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("producter", getProducter())
            .append("mrole", getMrole())
            .toString();
    }
}
