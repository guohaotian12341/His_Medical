package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 供应商对象 dzm_his_supplier
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
public class DzmHisSupplier extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String sid;

    /** 供应商名称 */
    @Excel(name = "供应商名称")
    private String supplierName;

    /** 联系人名称 */
    @Excel(name = "联系人名称")
    private String contactName;

    /** 联系人手机 */
    @Excel(name = "联系人手机")
    private String contactMobile;

    /** 联系人电话 */
    @Excel(name = "联系人电话")
    private String contactTelephone;

    /** 银行账号 */
    @Excel(name = "银行账号")
    private String bankAccount;

    /** 供应商地址 */
    @Excel(name = "供应商地址")
    private String address;

    /** 医院id */
    @Excel(name = "医院id")
    private String hospitalId;

    /** 角色 */
    @Excel(name = "角色")
    private String srole;

    @Excel(name = "创建时间")
    private Date createTime;

    @Excel(name = "修改时间")
    private Date updateTime;

    @Excel(name = "医院名称")
    private String hospitalName;

    @Excel(name = "id")
    private Integer id;

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public Date getUpdateTime() {
        return updateTime;
    }

    @Override
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }

    public String getSid() 
    {
        return sid;
    }
    public void setSupplierName(String supplierName) 
    {
        this.supplierName = supplierName;
    }

    public String getSupplierName() 
    {
        return supplierName;
    }
    public void setContactName(String contactName) 
    {
        this.contactName = contactName;
    }

    public String getContactName() 
    {
        return contactName;
    }
    public void setContactMobile(String contactMobile) 
    {
        this.contactMobile = contactMobile;
    }

    public String getContactMobile() 
    {
        return contactMobile;
    }
    public void setContactTelephone(String contactTelephone) 
    {
        this.contactTelephone = contactTelephone;
    }

    public String getContactTelephone() 
    {
        return contactTelephone;
    }
    public void setBankAccount(String bankAccount) 
    {
        this.bankAccount = bankAccount;
    }

    public String getBankAccount() 
    {
        return bankAccount;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setHospitalId(String hospitalId) 
    {
        this.hospitalId = hospitalId;
    }

    public String getHospitalId() 
    {
        return hospitalId;
    }
    public void setSrole(String srole) 
    {
        this.srole = srole;
    }

    public String getSrole() 
    {
        return srole;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("sid", getSid())
            .append("supplierName", getSupplierName())
            .append("contactName", getContactName())
            .append("contactMobile", getContactMobile())
            .append("contactTelephone", getContactTelephone())
            .append("bankAccount", getBankAccount())
            .append("address", getAddress())
            .append("hospitalId", getHospitalId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
                .append("hospitalName", getHospitalName())
                .append("id", getId())
            .append("srole", getSrole())
            .toString();
    }
}
