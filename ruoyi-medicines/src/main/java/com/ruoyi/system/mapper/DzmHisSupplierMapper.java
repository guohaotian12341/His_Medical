package com.ruoyi.system.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.DzmHisSupplier;

import java.util.List;

/**
 * 供应商Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisSupplierMapper 
{
    /**
     * 查询供应商
     * 
     * @param sid 供应商主键
     * @return 供应商
     */
    public DzmHisSupplier selectDzmHisSupplierBySid(String sid);

    /**
     * 查询供应商列表
     * 
     * @param dzmHisSupplier 供应商
     * @return 供应商集合
     */
    public List<DzmHisSupplier> selectDzmHisSupplierList(DzmHisSupplier dzmHisSupplier);

    /**
     * 新增供应商
     * 
     * @param dzmHisSupplier 供应商
     * @return 结果
     */
    public int insertDzmHisSupplier(DzmHisSupplier dzmHisSupplier);

    /**
     * 修改供应商
     * 
     * @param dzmHisSupplier 供应商
     * @return 结果
     */
    public int updateDzmHisSupplier(DzmHisSupplier dzmHisSupplier);

    /**
     * 删除供应商
     * 
     * @param sid 供应商主键
     * @return 结果
     */
    public int deleteDzmHisSupplierBySid(String sid);

    /**
     * 批量删除供应商
     * 
     * @param sids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisSupplierBySids(String[] sids);
}
