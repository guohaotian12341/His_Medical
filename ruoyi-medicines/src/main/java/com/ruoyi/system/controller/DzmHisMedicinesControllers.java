//package com.ruoyi.system.controller;
//
//import com.ruoyi.common.annotation.Log;
//import com.ruoyi.common.core.controller.BaseController;
//import com.ruoyi.common.core.domain.AjaxResult;
//import com.ruoyi.common.core.page.TableDataInfo;
//import com.ruoyi.common.enums.BusinessType;
//import com.ruoyi.common.utils.poi.ExcelUtil;
//import com.ruoyi.system.domain.DzmHisMedicines;
//import com.ruoyi.system.service.IDzmHisMedicinesService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//
///**
// * 药品信息Controller
// *
// * @author ruoyi
// * @date 2022-06-22
// */
//@RestController
//@RequestMapping("/system/purchase")
//public class DzmHisMedicinesControllers extends BaseController
//{
//    @Autowired
//    private IDzmHisMedicinesService dzmHisMedicinesService;
//
//    /**
//     * 查询药品信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:list')")
//    @GetMapping("/list")
//    public TableDataInfo list(DzmHisMedicines dzmHisMedicines)
//    {
//        startPage();
//        List<DzmHisMedicines> list = dzmHisMedicinesService.selectDzmHisMedicinesList(dzmHisMedicines);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出药品信息列表
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:export')")
//    @Log(title = "药品信息", businessType = BusinessType.EXPORT)
//    @PostMapping("/export")
//    public void export(HttpServletResponse response, DzmHisMedicines dzmHisMedicines)
//    {
//        List<DzmHisMedicines> list = dzmHisMedicinesService.selectDzmHisMedicinesList(dzmHisMedicines);
//        ExcelUtil<DzmHisMedicines> util = new ExcelUtil<DzmHisMedicines>(DzmHisMedicines.class);
//        util.exportExcel(response, list, "药品信息数据");
//    }
//
//    /**
//     * 获取药品信息详细信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:query')")
//    @GetMapping(value = "/{medicinesId}")
//    public AjaxResult getInfo(@PathVariable("medicinesId") String medicinesId)
//    {
//        return AjaxResult.success(dzmHisMedicinesService.selectDzmHisMedicinesByMedicinesId(medicinesId));
//    }
//
//    /**
//     * 新增药品信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:add')")
//    @Log(title = "药品信息", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody DzmHisMedicines dzmHisMedicines)
//    {
//        return toAjax(dzmHisMedicinesService.insertDzmHisMedicines(dzmHisMedicines));
//    }
//
//    /**
//     * 修改药品信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:edit')")
//    @Log(title = "药品信息", businessType = BusinessType.UPDATE)
//    @PutMapping
//    public AjaxResult edit(@RequestBody DzmHisMedicines dzmHisMedicines)
//    {
//        return toAjax(dzmHisMedicinesService.updateDzmHisMedicines(dzmHisMedicines));
//    }
//
//    /**
//     * 删除药品信息
//     */
//    @PreAuthorize("@ss.hasPermi('system:medicines:remove')")
//    @Log(title = "药品信息", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{medicinesIds}")
//    public AjaxResult remove(@PathVariable String[] medicinesIds)
//    {
//        return toAjax(dzmHisMedicinesService.deleteDzmHisMedicinesByMedicinesIds(medicinesIds));
//    }
//
//
//
//
//
//}
