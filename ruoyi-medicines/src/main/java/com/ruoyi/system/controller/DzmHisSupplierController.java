package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.DzmHisSupplier;
import com.ruoyi.system.service.IDzmHisSupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 供应商Controller
 * 
 * @author ruoyi
 * @date 2022-06-22
 */
@RestController
@RequestMapping("/system/supplier")
public class DzmHisSupplierController extends BaseController
{
    @Autowired
    private IDzmHisSupplierService dzmHisSupplierService;

    /**
     * 查询供应商列表
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisSupplier dzmHisSupplier)
    {
        startPage();
        List<DzmHisSupplier> list = dzmHisSupplierService.selectDzmHisSupplierList(dzmHisSupplier);
        return getDataTable(list);
    }

    /**
     * 导出供应商列表
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @Log(title = "供应商", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisSupplier dzmHisSupplier)
    {
        List<DzmHisSupplier> list = dzmHisSupplierService.selectDzmHisSupplierList(dzmHisSupplier);
        ExcelUtil<DzmHisSupplier> util = new ExcelUtil<DzmHisSupplier>(DzmHisSupplier.class);
        util.exportExcel(response, list, "供应商数据");
    }

    /**
     * 获取供应商详细信息
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping(value = "/{sid}")
    public AjaxResult getInfo(@PathVariable("sid") String sid)
    {
        return AjaxResult.success(dzmHisSupplierService.selectDzmHisSupplierBySid(sid));
    }

    /**
     * 新增供应商
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @Log(title = "供应商", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisSupplier dzmHisSupplier)
    {
        return toAjax(dzmHisSupplierService.insertDzmHisSupplier(dzmHisSupplier));
    }

    /**
     * 修改供应商
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @Log(title = "供应商", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisSupplier dzmHisSupplier)
    {
        return toAjax(dzmHisSupplierService.updateDzmHisSupplier(dzmHisSupplier));
    }

    /**
     * 删除供应商
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @Log(title = "供应商", businessType = BusinessType.DELETE)
	@DeleteMapping("/{sids}")
    public AjaxResult remove(@PathVariable String[] sids)
    {
        return toAjax(dzmHisSupplierService.deleteDzmHisSupplierBySids(sids));
    }
}
