package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.system.domain.BatchesOfInventoryInfo;
import com.ruoyi.system.domain.DzmHisMedicines;
import com.ruoyi.system.domain.PurchaseInfo;
import com.ruoyi.system.service.IPurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/purchase/")
@DataSource(DataSourceType.SLAVE)
public class PurchaseController extends BaseController {
    @Autowired
    private IPurchaseService service;

    /**
     * id查询药品信息
     * @param
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/querylist")
    public Object list(String arrs[])
    {

        List<DzmHisMedicines> list = service.querylist(arrs);
       return list;
    }

    /**
     *查询药品
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/queryMedicines")
    public Object queryMedicines(){
        return service.queryMedicines();
    }

    /**
     * 查询供应商
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/getSupplier")
    public Object getSupplier(){
        return service.getSupplier();
    }

    /**
     * 添加采购信息
     * @param purchaseInfo
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PostMapping ("/addpurchase")
    public Integer addpurchase(@RequestBody PurchaseInfo purchaseInfo){
        System.out.println("添加采购信息"+purchaseInfo);
        return service.addpurchase(purchaseInfo);
    }

    /**
     * 生成编号
     * @param
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping ("/createid")
    public String createID(){
      return service.createID();
    }

    /**
     * 添加批次库存信息
     * @param batches
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PostMapping ("/addbatchesinventory")
    public Integer addbatchesinventory(@RequestBody BatchesOfInventoryInfo batches){
        System.out.println("批次库存信息"+batches);
        return service.addBatchesInventory(batches);
    }





}
