package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;

import com.ruoyi.system.domain.MedicinesInfo;
import com.ruoyi.system.service.IMedicinesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/medicines/")
public class MedicinesController extends BaseController {

    @Autowired
    IMedicinesService ms;

    /**
     *查询药品信息
     * @param medicines
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/querylist")
    public TableDataInfo list(MedicinesInfo medicines)
    {
        startPage();
        List<MedicinesInfo> list = ms.querylist(medicines);
        return getDataTable(list);
    }

    /**
     * 删除药品信息
     * @param  medicinesId
     * @return
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @GetMapping("/del")
    public Integer dle(Integer medicinesId)
    {
        System.out.println("删除ID-------------->"+medicinesId);
        return ms.del(medicinesId);
    }


    /**
     * 导入模板
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PostMapping ("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<MedicinesInfo> util = new ExcelUtil<>(MedicinesInfo.class);
        util.importTemplateExcel(response, "药品数据");
    }

    /**
     * 导入数据
     */
    @Log(title = "药品信息", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception {
        ExcelUtil<MedicinesInfo> util = new ExcelUtil<>(MedicinesInfo.class);
        List<MedicinesInfo> medicinesList = util.importExcel(file.getInputStream());
        System.out.println(medicinesList);
        String operName = getUsername();
        String message = ms.importData(medicinesList,updateSupport,operName);
        return AjaxResult.success(message);
    }

    /**
     * 药品导出
     * @param response
     * @param medicinesInfo
     */
    @Log(title = "药品信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('purchase')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, MedicinesInfo medicinesInfo)
    {
        List<MedicinesInfo> list = ms.medicinesList(medicinesInfo);
        ExcelUtil<MedicinesInfo> util = new ExcelUtil<>(MedicinesInfo.class);
        util.exportExcel(response, list, "药品数据");
    }

}
