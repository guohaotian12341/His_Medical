package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.service.DepartmentIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: DepartmentHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/6/28 9:44
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/department")
public class DepartmentHandler {
    @Autowired
    private DepartmentIService service;

    @RequestMapping("/findAllDepartmentName")
    public Object findAllDepartmentName(){
        return service.findAll();
    }

}
