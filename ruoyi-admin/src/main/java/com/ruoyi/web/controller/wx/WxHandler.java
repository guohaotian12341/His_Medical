package com.ruoyi.web.controller.wx;


import com.ruoyi.edu.service.WxOpenIdIService;
import com.ruoyi.edu.domain.WxOpenIdVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: WxHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/6/17 9:52
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */


@RestController
@RequestMapping("/wx")
public class WxHandler {
    @Value("${wx.appid}")
    private String appid;
    @Value("${wx.appSecret}")
    private String appSecret;

    @Autowired
    private WxOpenIdIService service;


    /**
     * @Author:guohaotian
     * @Description: 获取请求地址
     * @Param:code
     * @Return:java.lang.String
     * @Date:2022/6/17~14:16
     */
    @RequestMapping("/getResult")
    public String getResult(String code){
        System.out.println(code);
        return "https://api.weixin.qq.com/sns/jscode2session?appid="+appid+"&secret="+appSecret+"&js_code="+code+"&grant_type=authorization_code";
    }


}
