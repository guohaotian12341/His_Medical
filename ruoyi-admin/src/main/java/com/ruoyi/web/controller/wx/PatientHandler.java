package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.PatientVo;
import com.ruoyi.edu.service.PatientIService;
import com.ruoyi.edu.service.WxOpenIdIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: PatientHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/6/18 9:05
 * @Description:患者用户
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/patient")
public class PatientHandler {
    @Autowired
    private PatientIService service;

    @Autowired
    private WxOpenIdIService wxOpenIdIService;

    @RequestMapping("/queryPatient")
    public Object queryPatient(@RequestParam(required = false) String openId){
        List<PatientVo> patientVos = service.QueryByOpenId(openId);
        System.out.println(patientVos);
        return patientVos;
    }

    @RequestMapping("/findByPatient")
    public Object findByPatient(String patientId){
        return service.findByPatientId(patientId);
    }

    @RequestMapping("/DelPatient")
    public Object DelPatient(PatientVo patientVo){
        return service.DelPatient(patientVo);
    }

    @RequestMapping("/insertPatient")
    public Object insertPatient(PatientVo patientVo){
//        获取当前时间戳
        long l = System.currentTimeMillis()/1000;
        patientVo.setCreateTime(l);
        patientVo.setUpdateTime(l);
        return service.InsertPatient(patientVo);
    }

}
