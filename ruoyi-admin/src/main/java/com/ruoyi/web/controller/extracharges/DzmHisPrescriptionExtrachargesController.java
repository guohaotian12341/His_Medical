package com.ruoyi.web.controller.extracharges;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DzmHisPrescriptionExtracharges;
import com.ruoyi.system.service.IDzmHisPrescriptionExtrachargesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 处方附加费用Controller
 * 
 * @author ruoyi
 * @date 2022-06-17
 */
@RestController
@RequestMapping("/system/extracharges")
public class DzmHisPrescriptionExtrachargesController extends BaseController
{
    @Autowired
    private IDzmHisPrescriptionExtrachargesService dzmHisPrescriptionExtrachargesService;

    /**
     * 查询处方附加费用列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        startPage();
        List<DzmHisPrescriptionExtracharges> list = dzmHisPrescriptionExtrachargesService.selectDzmHisPrescriptionExtrachargesList(dzmHisPrescriptionExtracharges);
        return getDataTable(list);
    }

    /**
     * 导出处方附加费用列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "处方附加费用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        List<DzmHisPrescriptionExtracharges> list = dzmHisPrescriptionExtrachargesService.selectDzmHisPrescriptionExtrachargesList(dzmHisPrescriptionExtracharges);
        ExcelUtil<DzmHisPrescriptionExtracharges> util = new ExcelUtil<DzmHisPrescriptionExtracharges>(DzmHisPrescriptionExtracharges.class);
        util.exportExcel(response, list, "处方附加费用数据");
    }

    /**
     * 获取处方附加费用详细信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping(value = "/{preId}")
    public AjaxResult getInfo(@PathVariable("preId") String preId)
    {
        return AjaxResult.success(dzmHisPrescriptionExtrachargesService.selectDzmHisPrescriptionExtrachargesByPreId(preId));
    }

    /**
     * 新增处方附加费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "处方附加费用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        return toAjax(dzmHisPrescriptionExtrachargesService.insertDzmHisPrescriptionExtracharges(dzmHisPrescriptionExtracharges));
    }

    /**
     * 修改处方附加费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "处方附加费用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisPrescriptionExtracharges dzmHisPrescriptionExtracharges)
    {
        return toAjax(dzmHisPrescriptionExtrachargesService.updateDzmHisPrescriptionExtracharges(dzmHisPrescriptionExtracharges));
    }

    /**
     * 删除处方附加费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "处方附加费用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{preIds}")
    public AjaxResult remove(@PathVariable String[] preIds)
    {
        return toAjax(dzmHisPrescriptionExtrachargesService.deleteDzmHisPrescriptionExtrachargesByPreIds(preIds));
    }
}
