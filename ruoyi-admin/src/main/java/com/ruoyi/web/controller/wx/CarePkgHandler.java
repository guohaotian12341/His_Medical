package com.ruoyi.web.controller.wx;

import com.ruoyi.edu.domain.CarePayLogVo;
import com.ruoyi.edu.domain.CarePkgVo;
import com.ruoyi.edu.service.CarePkgIService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Created by IntelliJ IDEA
 * @Class_Name: CarePkgHandler
 * @package_Name: com.ruoyi.web.controller.wx
 * @User: guohaotian
 * @Date: 2022/7/1 14:26
 * @Description:
 * @To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/carePkg")
public class CarePkgHandler {
    @Autowired
    private CarePkgIService service;

    @RequestMapping("/queryCarePkgById")
    public Object queryCarePkgById(CarePkgVo carePkgVo){
        return service.queryCarePkgById(carePkgVo);
    }

    @RequestMapping("/updateStatus")
    public Object updateStatus(CarePkgVo carePkgVo){
        return service.updateStatus(carePkgVo);
    }
}
