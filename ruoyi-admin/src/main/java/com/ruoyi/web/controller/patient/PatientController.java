package com.ruoyi.web.controller.patient;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;

import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.PafileInfo;
import com.ruoyi.system.domain.PatientInfo;
import com.ruoyi.system.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/system/patient")
public class PatientController extends BaseController {

    @Autowired
    @Qualifier("LPatientServiceImpl")
    private PatientService patientService;

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @GetMapping("/list")
    public Object querylist(PatientInfo pi){
        startPage();
        List<PatientInfo> list=patientService.querylist(pi);
        return this.getDataTable(list);
    }

    @Log(title = "患者信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, PatientInfo patientInfo)
    {
        List<PatientInfo> list = patientService.selectPatientList(patientInfo);
        ExcelUtil<PatientInfo> util = new ExcelUtil<PatientInfo>(PatientInfo.class);
        util.exportExcel(response, list, "患者信息");
    }


    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @GetMapping(value = "/{patientId}")
    public Object filelist(@PathVariable("patientId") Integer patientId){
        return patientService.filelist(patientId);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @PutMapping("/updatePatient")
    public Integer updatePatient(@Validated @RequestBody PatientInfo patientInfo){
        return patientService.updatePatient(patientInfo);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @DeleteMapping("/{patientId}")
    public Integer deletePatient(@PathVariable Integer patientId){
        return patientService.deletePatient(patientId);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @PostMapping("/addPatient")
    public Object addPatient(@Validated @RequestBody PafileInfo pf){
        return patientService.addPatient(pf);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @GetMapping ("/LHH/{patientId}")
    public Object All(@PathVariable Integer patientId){
        return patientService.All(patientId);
    }

    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @PutMapping("/updatePafie/{patientId}")
    public Integer updatePafie(@PathVariable long patientId){
        return patientService.updatePafie(patientId);
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('dean') || @ss.hasPermi('chief')")
    @PutMapping("/upPatient/{patientId}")
    public Integer upPatient(@PathVariable long patientId){
        return patientService.upPatient(patientId);
    }
}
