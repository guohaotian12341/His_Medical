package com.ruoyi.web.controller.instpectionfee;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.DzmHisInspectionfee;
import com.ruoyi.system.service.IDzmHisInspectionfeeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 检查项目费用Controller
 * 
 * @author ruoyi
 * @date 2022-06-18
 */
@RestController
@RequestMapping("/system/inspectionfee")
public class DzmHisInspectionfeeController extends BaseController
{
    @Autowired
    private IDzmHisInspectionfeeService dzmHisInspectionfeeService;

    /**
     * 查询检查项目费用列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisInspectionfee dzmHisInspectionfee)
    {
        startPage();
        List<DzmHisInspectionfee> list = dzmHisInspectionfeeService.selectDzmHisInspectionfeeList(dzmHisInspectionfee);
        return getDataTable(list);
    }

    /**
     * 导出检查项目费用列表
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "检查项目费用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DzmHisInspectionfee dzmHisInspectionfee)
    {
        List<DzmHisInspectionfee> list = dzmHisInspectionfeeService.selectDzmHisInspectionfeeList(dzmHisInspectionfee);
        ExcelUtil<DzmHisInspectionfee> util = new ExcelUtil<DzmHisInspectionfee>(DzmHisInspectionfee.class);
        util.exportExcel(response, list, "检查项目费用数据");
    }

    /**
     * 获取检查项目费用详细信息
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @GetMapping(value = "/{insId}")
    public AjaxResult getInfo(@PathVariable("insId") String insId)
    {
        return AjaxResult.success(dzmHisInspectionfeeService.selectDzmHisInspectionfeeByInsId(insId));
    }

    /**
     * 新增检查项目费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "检查项目费用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisInspectionfee dzmHisInspectionfee)
    {
        return toAjax(dzmHisInspectionfeeService.insertDzmHisInspectionfee(dzmHisInspectionfee));
    }

    /**
     * 修改检查项目费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "检查项目费用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DzmHisInspectionfee dzmHisInspectionfee)
    {
        return toAjax(dzmHisInspectionfeeService.updateDzmHisInspectionfee(dzmHisInspectionfee));
    }

    /**
     * 删除检查项目费用
     */
    @PreAuthorize("@ss.hasPermi('dean')")
    @Log(title = "检查项目费用", businessType = BusinessType.DELETE)
	@DeleteMapping("/{insIds}")
    public AjaxResult remove(@PathVariable String[] insIds)
    {
        return toAjax(dzmHisInspectionfeeService.deleteDzmHisInspectionfeeByInsIds(insIds));
    }
}
