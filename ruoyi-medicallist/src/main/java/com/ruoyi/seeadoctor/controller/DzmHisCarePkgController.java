package com.ruoyi.seeadoctor.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.seeadoctor.domain.DzmHisCarePkg;
import com.ruoyi.seeadoctor.service.IDzmHisCarePkgService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 收费总Controller
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
@RestController
@RequestMapping("/system/pkg")
public class DzmHisCarePkgController extends BaseController
{
    @Autowired
    private IDzmHisCarePkgService dzmHisCarePkgService;



    /**
     * 新增收费总
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "收费总", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DzmHisCarePkg dzmHisCarePkg)
    {
        dzmHisCarePkgService.insertDzmHisCarePkg(dzmHisCarePkg);
        return AjaxResult.success(dzmHisCarePkg.getId());
    }
}
