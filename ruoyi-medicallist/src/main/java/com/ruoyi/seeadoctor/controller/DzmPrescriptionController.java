package com.ruoyi.seeadoctor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.domain.DzmPrescription;
import com.ruoyi.seeadoctor.service.IDzmPatientService;
import com.ruoyi.seeadoctor.service.IDzmPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 药品厂库Controller
 *
 * @author fyy
 * @date 2022-06-17
 */

@RestController
@RequestMapping("/sickness/prescription")
public class DzmPrescriptionController extends BaseController {
    @Autowired
    private IDzmPrescriptionService dzmPrescriptionService;

    //药品信息表
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/druglist")
    public TableDataInfo druglist(DzmPrescription dzmPrescription)
    {
        startPage();
        List<DzmPrescription> list = dzmPrescriptionService.selectDzmDrugList(dzmPrescription);
        return getDataTable(list);
    }
    //检查项目表
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/examinelist")
    public TableDataInfo examinelist(DzmPrescription dzmPrescription)
    {
        startPage();
        List<DzmPrescription> list = dzmPrescriptionService.selectDzmExamineList(dzmPrescription);
        return getDataTable(list);
    }

    //药品ID获取信息
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
	@GetMapping("/drug/{medicinesIds}")
    public TableDataInfo drug(@PathVariable Long[] medicinesIds)
    {
        List<DzmPrescription> list = dzmPrescriptionService.selectDzmPatientByMedicinesId(medicinesIds);
        return getDataTable(list);
    }

    //检查项目ID获取信息
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/examine/{insIds}")
    public TableDataInfo examine(@PathVariable Long[] insIds)
    {
        List<DzmPrescription> list = dzmPrescriptionService.selectDzmPatientByinsIds(insIds);
        return getDataTable(list);
    }
}
