package com.ruoyi.seeadoctor.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.seeadoctor.domain.DzmHisRegistration;
import com.ruoyi.seeadoctor.service.IDzmHisRegistrationService;
import com.ruoyi.seeadoctor.domain.DzmHisRegistration;
import com.ruoyi.seeadoctor.service.IDzmHisRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 门诊挂号Controller
 * 
 * @author fyy
 * @date 2022-06-17
 */
@RestController
@RequestMapping("/sickness/registration")
public class DzmHisRegistrationController extends BaseController
{
    @Autowired
    private IDzmHisRegistrationService dzmHisRegistrationService;

    /**
     * 查询门诊挂号列表
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/list")
    public TableDataInfo list(DzmHisRegistration dzmHisRegistration)
    {
        startPage();
        List<DzmHisRegistration> list = dzmHisRegistrationService.selectDzmHisRegistrationList(dzmHisRegistration);
        return getDataTable(list);
    }

    /**
     * 获取门诊挂号详细信息
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping(value = "/{registrationId}")
    public AjaxResult getInfo(@PathVariable("registrationId") Long registrationId)
    {
        return AjaxResult.success(dzmHisRegistrationService.selectDzmHisRegistrationByRegistrationId(registrationId));
    }

    /**
     * 修改门诊挂号状态
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @Log(title = "门诊挂号", businessType = BusinessType.UPDATE)
    @PutMapping("/update/{registrationId}")
    public AjaxResult update(@PathVariable long registrationId)
    {
        return toAjax(dzmHisRegistrationService.updateStatus(registrationId));
    }

}
