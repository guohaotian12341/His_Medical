package com.ruoyi.seeadoctor.mapper;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCareHistory;

/**
 * 历史病历Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisCareHistoryMapper 
{
    /**
     * 查询历史病历
     * 
     * @param
     * @return 历史病历
     */
    public List<DzmHisCareHistory> selectDzmHisCareHistoryById(Integer patientId);

    DzmHisCareHistory selectHistoryById(Integer id);



    /**
     * 修改历史病历
     * 
     * @param dzmHisCareHistory 历史病历
     * @return 结果
     */
    public int updateDzmHisCareHistory(DzmHisCareHistory dzmHisCareHistory);

}
