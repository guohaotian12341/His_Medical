package com.ruoyi.seeadoctor.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmPatient;

import java.util.List;

/**
 * 患者用户Mapper接口
 * 
 * @author fyy
 * @date 2022-06-17
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmPatientMapper 
{

    /**
     * 查询患者用户列表
     * 
     * @param dzmPatient 患者用户
     * @return 患者用户集合
     */
    public List<DzmPatient> selectDzmPatientList(DzmPatient dzmPatient);


}
