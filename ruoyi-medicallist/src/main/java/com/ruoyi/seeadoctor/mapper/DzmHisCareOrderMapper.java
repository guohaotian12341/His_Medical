package com.ruoyi.seeadoctor.mapper;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCareOrder;

import java.util.List;

/**
 * 处方列Mapper接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisCareOrderMapper 
{
    /**
     * 查询处方列
     * 
     * @param id 处方列主键
     * @return 处方列
     */
    public DzmHisCareOrder selectDzmHisCareOrderById(Integer id);

    /**
     * 查询处方列列表
     * 
     * @param dzmHisCareOrder 处方列
     * @return 处方列集合
     */
    public List<DzmHisCareOrder> selectDzmHisCareOrderList(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 新增处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    public int insertDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 修改处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    public int updateDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder);

    /**
     * 删除处方列
     * 
     * @param id 处方列主键
     * @return 结果
     */
    public int deleteDzmHisCareOrderById(Integer id);

    /**
     * 批量删除处方列
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDzmHisCareOrderByIds(Integer[] ids);
}
