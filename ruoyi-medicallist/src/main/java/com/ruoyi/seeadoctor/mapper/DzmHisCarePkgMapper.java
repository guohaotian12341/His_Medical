package com.ruoyi.seeadoctor.mapper;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCarePkg;

/**
 * 收费总Mapper接口
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
@DataSource(DataSourceType.SLAVE)
public interface DzmHisCarePkgMapper 
{


    /**
     * 新增收费总
     * 
     * @param dzmHisCarePkg 收费总
     * @return 结果
     */
    public int insertDzmHisCarePkg(DzmHisCarePkg dzmHisCarePkg);


}
