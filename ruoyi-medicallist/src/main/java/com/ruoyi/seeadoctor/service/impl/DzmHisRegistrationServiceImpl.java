package com.ruoyi.seeadoctor.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.seeadoctor.domain.DzmHisRegistration;
import com.ruoyi.seeadoctor.mapper.DzmHisRegistrationMapper;
import com.ruoyi.seeadoctor.service.IDzmHisRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 门诊挂号Service业务层处理
 * 
 * @author fyy
 * @date 2022-06-17
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisRegistrationServiceImpl implements IDzmHisRegistrationService
{
    @Autowired
    private DzmHisRegistrationMapper dzmHisRegistrationMapper;

    /**
     * 查询门诊挂号
     * 
     * @param registrationId 门诊挂号主键
     * @return 门诊挂号
     */
    @Override
    public DzmHisRegistration selectDzmHisRegistrationByRegistrationId(Long registrationId)
    {
        return dzmHisRegistrationMapper.selectDzmHisRegistrationByRegistrationId(registrationId);
    }

    /**
     * 查询门诊挂号列表
     * 
     * @param dzmHisRegistration 门诊挂号
     * @return 门诊挂号
     */
    @Override
    public List<DzmHisRegistration> selectDzmHisRegistrationList(DzmHisRegistration dzmHisRegistration)
    {
        return dzmHisRegistrationMapper.selectDzmHisRegistrationList(dzmHisRegistration);
    }

    @Override
    public int updateStatus(long registrationId) {
        return dzmHisRegistrationMapper.updateStatus(registrationId);
    }

}
