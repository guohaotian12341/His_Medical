package com.ruoyi.seeadoctor.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.domain.DzmPrescription;
import com.ruoyi.seeadoctor.mapper.DzmPatientMapper;
import com.ruoyi.seeadoctor.mapper.DzmPrescriptionMapper;
import com.ruoyi.seeadoctor.service.IDzmPrescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@DataSource(DataSourceType.SLAVE)
public class DzmPrescriptionServiceImpl implements IDzmPrescriptionService {

    @Autowired
    private DzmPrescriptionMapper dzmPrescriptionMapper;

    @Override
    public List<DzmPrescription> selectDzmDrugList(DzmPrescription dzmPrescription) {
        return dzmPrescriptionMapper.selectDzmDrugList(dzmPrescription);
    }

    @Override
    public List<DzmPrescription> selectDzmExamineList(DzmPrescription dzmPrescription) {
        return dzmPrescriptionMapper.selectDzmExamineList(dzmPrescription);
    }

    @Override
    public List<DzmPrescription> selectDzmPatientByMedicinesId(Long[] medicinesIds) {
        return dzmPrescriptionMapper.selectDzmPatientByMedicinesId(medicinesIds);
    }

    @Override
    public List<DzmPrescription> selectDzmPatientByinsIds(Long[] insIds) {
        return dzmPrescriptionMapper.selectDzmPatientByinsIds(insIds);
    }
}
