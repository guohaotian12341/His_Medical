package com.ruoyi.seeadoctor.service;

import java.util.List;

import com.ruoyi.seeadoctor.domain.DzmHisCarePkg;

/**
 * 收费总Service接口
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
public interface IDzmHisCarePkgService 
{
    /**
     * 新增收费总
     * 
     * @param dzmHisCarePkg 收费总
     * @return 结果
     */
    public int insertDzmHisCarePkg(DzmHisCarePkg dzmHisCarePkg);

}
