package com.ruoyi.seeadoctor.service;

import java.util.List;

import com.ruoyi.seeadoctor.domain.DzmHisCareHistory;

/**
 * 历史病历Service接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
public interface IDzmHisCareHistoryService 
{
    /**
     * 查询历史病历
     * 
     * @param
     * @return 历史病历
     */
    public List<DzmHisCareHistory> selectDzmHisCareHistoryById(Integer patientId);

    DzmHisCareHistory selectHistoryById(Integer id);

    /**
     * 修改历史病历
     * 
     * @param dzmHisCareHistory 历史病历
     * @return 结果
     */
    public int updateDzmHisCareHistory(DzmHisCareHistory dzmHisCareHistory);

}
