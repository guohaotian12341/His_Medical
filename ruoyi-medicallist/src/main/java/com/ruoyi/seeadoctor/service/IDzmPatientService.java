package com.ruoyi.seeadoctor.service;

import com.ruoyi.seeadoctor.domain.DzmPatient;

import java.util.List;

/**
 * 患者用户Service接口
 * 
 * @author fyy
 * @date 2022-06-17
 */
public interface IDzmPatientService 
{
    /**
     * 查询患者用户
     * 
     * @param patientId 患者用户主键
     * @return 患者用户
     */
//    public DzmPatient selectDzmPatientByPatientId(Long patientId);

    /**
     * 查询患者用户列表
     * 
     * @param dzmPatient 患者用户
     * @return 患者用户集合
     */
    public List<DzmPatient> selectDzmPatientList(DzmPatient dzmPatient);

}
