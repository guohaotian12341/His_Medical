package com.ruoyi.seeadoctor.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.seeadoctor.domain.DzmPatient;
import com.ruoyi.seeadoctor.mapper.DzmPatientMapper;
import com.ruoyi.seeadoctor.service.IDzmPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 患者用户Service业务层处理
 * 
 * @author fyy
 * @date 2022-06-17
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmPatientServiceImpl implements IDzmPatientService
{
    @Autowired
    private DzmPatientMapper dzmPatientMapper;

    /**
     * 查询患者用户列表
     * 
     * @param dzmPatient 患者用户
     * @return 患者用户
     */
    @Override
    public List<DzmPatient> selectDzmPatientList(DzmPatient dzmPatient)
    {
        return dzmPatientMapper.selectDzmPatientList(dzmPatient);
    }
}
