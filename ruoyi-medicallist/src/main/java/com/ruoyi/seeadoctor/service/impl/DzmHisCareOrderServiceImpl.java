package com.ruoyi.seeadoctor.service.impl;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCareOrder;
import com.ruoyi.seeadoctor.mapper.DzmHisCareOrderMapper;
import com.ruoyi.seeadoctor.service.IDzmHisCareOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 处方列Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisCareOrderServiceImpl implements IDzmHisCareOrderService
{
    @Autowired
    private DzmHisCareOrderMapper dzmHisCareOrderMapper;

    /**
     * 查询处方列
     * 
     * @param id 处方列主键
     * @return 处方列
     */
    @Override
    public DzmHisCareOrder selectDzmHisCareOrderById(Integer id)
    {
        return dzmHisCareOrderMapper.selectDzmHisCareOrderById(id);
    }

    /**
     * 查询处方列列表
     * 
     * @param dzmHisCareOrder 处方列
     * @return 处方列
     */
    @Override
    public List<DzmHisCareOrder> selectDzmHisCareOrderList(DzmHisCareOrder dzmHisCareOrder)
    {
        return dzmHisCareOrderMapper.selectDzmHisCareOrderList(dzmHisCareOrder);
    }

    /**
     * 新增处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    @Override
    public int insertDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder)
    {
        return dzmHisCareOrderMapper.insertDzmHisCareOrder(dzmHisCareOrder);
    }

    /**
     * 修改处方列
     * 
     * @param dzmHisCareOrder 处方列
     * @return 结果
     */
    @Override
    public int updateDzmHisCareOrder(DzmHisCareOrder dzmHisCareOrder)
    {
        return dzmHisCareOrderMapper.updateDzmHisCareOrder(dzmHisCareOrder);
    }

    /**
     * 批量删除处方列
     * 
     * @param ids 需要删除的处方列主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisCareOrderByIds(Integer[] ids)
    {
        return dzmHisCareOrderMapper.deleteDzmHisCareOrderByIds(ids);
    }

    /**
     * 删除处方列信息
     * 
     * @param id 处方列主键
     * @return 结果
     */
    @Override
    public int deleteDzmHisCareOrderById(Integer id)
    {
        return dzmHisCareOrderMapper.deleteDzmHisCareOrderById(id);
    }
}
