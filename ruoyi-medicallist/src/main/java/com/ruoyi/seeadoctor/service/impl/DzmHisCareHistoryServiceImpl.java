package com.ruoyi.seeadoctor.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCareHistory;
import com.ruoyi.seeadoctor.mapper.DzmHisCareHistoryMapper;
import com.ruoyi.seeadoctor.service.IDzmHisCareHistoryService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 历史病历Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisCareHistoryServiceImpl implements IDzmHisCareHistoryService
{
    @Autowired
    private DzmHisCareHistoryMapper dzmHisCareHistoryMapper;

    /**
     * 查询历史病历
     * 
     * @param
     * @return 历史病历
     */
    @Override
    public List<DzmHisCareHistory> selectDzmHisCareHistoryById(Integer patientId)
    {
        return dzmHisCareHistoryMapper.selectDzmHisCareHistoryById(patientId);
    }

    @Override
    public DzmHisCareHistory selectHistoryById(Integer id) {
        return dzmHisCareHistoryMapper.selectHistoryById(id);
    }





    /**
     * 修改历史病历
     * 
     * @param dzmHisCareHistory 历史病历
     * @return 结果
     */
    @Override
    public int updateDzmHisCareHistory(DzmHisCareHistory dzmHisCareHistory)
    {
        return dzmHisCareHistoryMapper.updateDzmHisCareHistory(dzmHisCareHistory);
    }



}
