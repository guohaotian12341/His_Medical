package com.ruoyi.seeadoctor.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.seeadoctor.domain.DzmHisCarePkg;
import com.ruoyi.seeadoctor.mapper.DzmHisCarePkgMapper;
import com.ruoyi.seeadoctor.service.IDzmHisCarePkgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * 收费总Service业务层处理
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
@Service
@DataSource(DataSourceType.SLAVE)
public class DzmHisCarePkgServiceImpl implements IDzmHisCarePkgService
{
    @Autowired
    private DzmHisCarePkgMapper dzmHisCarePkgMapper;


    /**
     * 新增收费总
     * 
     * @param dzmHisCarePkg 收费总
     * @return 结果
     */
    @Override
    public int insertDzmHisCarePkg(DzmHisCarePkg dzmHisCarePkg)
    {
        return dzmHisCarePkgMapper.insertDzmHisCarePkg(dzmHisCarePkg);
    }

}
