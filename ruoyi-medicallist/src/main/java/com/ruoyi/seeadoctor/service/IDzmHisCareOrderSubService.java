package com.ruoyi.seeadoctor.service;

import java.util.List;
import java.util.Map;

/**
 * 开诊用药明细Service接口
 * 
 * @author ruoyi
 * @date 2022-06-28
 */
public interface IDzmHisCareOrderSubService 
{

    /**
     * 新增开诊用药明细
     * 
     * @param
     * @return 结果
     */
    public int insertDzmHisCareOrderSub(List<Map> maps);


}
