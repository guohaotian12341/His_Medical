package com.ruoyi.seeadoctor.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收费总对象 dzm_his_care_pkg
 * 
 * @author ruoyi
 * @date 2022-07-08
 */
public class DzmHisCarePkg extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String hospitalId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String doctorId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String patientId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String careHistoryId;

    /** 挂号ID */
    @Excel(name = "挂号ID")
    private String registrationId;

    /** 商户订单号 */
    @Excel(name = "商户订单号")
    private String orderCode;

    /** 应付金额 */
    @Excel(name = "应付金额")
    private BigDecimal amount;

    /** 在线支付部分 */
    @Excel(name = "在线支付部分")
    private BigDecimal olPayPart;

    /** 收费类型：0就诊处，1挂号处，2问答，3... */
    @Excel(name = "收费类型：0就诊处，1挂号处，2问答，3...")
    private String typeId;

    /** 状态:0未支付，1已支付，2确认收款，3申请退款，4已退款,5部分支付,6完成交易（如：已发药），7部分退款 */
    @Excel(name = "状态:0未支付，1已支付，2确认收款，3申请退款，4已退款,5部分支付,6完成交易", readConverterExp = "如=：已发药")
    private String status;

    /** 插入时间 */
    @Excel(name = "插入时间")
    private String addtime;

    /** 操作地点：1售药，2查检项目，3附加费用，4挂号，，，， */
    @Excel(name = "操作地点：1售药，2查检项目，3附加费用，4挂号，，，，")
    private String opPlace;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setHospitalId(String hospitalId) 
    {
        this.hospitalId = hospitalId;
    }

    public String getHospitalId() 
    {
        return hospitalId;
    }
    public void setDoctorId(String doctorId) 
    {
        this.doctorId = doctorId;
    }

    public String getDoctorId() 
    {
        return doctorId;
    }
    public void setPatientId(String patientId) 
    {
        this.patientId = patientId;
    }

    public String getPatientId() 
    {
        return patientId;
    }
    public void setCareHistoryId(String careHistoryId) 
    {
        this.careHistoryId = careHistoryId;
    }

    public String getCareHistoryId() 
    {
        return careHistoryId;
    }
    public void setRegistrationId(String registrationId) 
    {
        this.registrationId = registrationId;
    }

    public String getRegistrationId() 
    {
        return registrationId;
    }
    public void setOrderCode(String orderCode) 
    {
        this.orderCode = orderCode;
    }

    public String getOrderCode() 
    {
        return orderCode;
    }
    public void setAmount(BigDecimal amount) 
    {
        this.amount = amount;
    }

    public BigDecimal getAmount() 
    {
        return amount;
    }
    public void setOlPayPart(BigDecimal olPayPart) 
    {
        this.olPayPart = olPayPart;
    }

    public BigDecimal getOlPayPart() 
    {
        return olPayPart;
    }
    public void setTypeId(String typeId) 
    {
        this.typeId = typeId;
    }

    public String getTypeId() 
    {
        return typeId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setAddtime(String addtime) 
    {
        this.addtime = addtime;
    }

    public String getAddtime() 
    {
        return addtime;
    }
    public void setOpPlace(String opPlace) 
    {
        this.opPlace = opPlace;
    }

    public String getOpPlace() 
    {
        return opPlace;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hospitalId", getHospitalId())
            .append("doctorId", getDoctorId())
            .append("patientId", getPatientId())
            .append("careHistoryId", getCareHistoryId())
            .append("registrationId", getRegistrationId())
            .append("orderCode", getOrderCode())
            .append("amount", getAmount())
            .append("olPayPart", getOlPayPart())
            .append("typeId", getTypeId())
            .append("status", getStatus())
            .append("addtime", getAddtime())
            .append("opPlace", getOpPlace())
            .toString();
    }
}
