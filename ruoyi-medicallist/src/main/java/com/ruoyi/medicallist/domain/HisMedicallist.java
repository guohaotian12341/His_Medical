package com.ruoyi.medicallist.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;

import lombok.Data;

@Data
public class HisMedicallist {

    /**
     * 门诊挂号对象 dzm_his_registration
     *
     * @author ruoyi
     * @date 2022-06-18
     */
    private static final long serialVersionUID = 1L;

    //序号
    private Integer registrationId;
    //挂号单号
    private long registrationNumber;
    //患者姓名
    private String name;
    //患者性别1男2女
    private String sex;
    //患者年龄
    private Integer age;
    //患者电话
    private String mobile;
    //科室名称
    private String departmentName;
    //接诊医生
    private String truename;
    //接诊时间1
    private String updateTime;
    //接诊时间2
    private String updateTimes;
    //挂号状态,1为待就诊，3为已退号，2为已就诊,4为作废，5为未付款，6为部分支付
    private Integer registrationStatus;

    //挂号退款
    //挂号费用名称
    private String registeredfeeName;
    //挂号费用总金额
    private Integer registeredfeeAggregateAmount;

//    public Integer getRegistrationId() {
//        return registrationId;
//    }
//
//    public void setRegistrationId(Integer registrationId) {
//        this.registrationId = registrationId;
//    }
//
//    public long getRegistrationNumber() {
//        return registrationNumber;
//    }
//
//    public void setRegistrationNumber(long registrationNumber) {
//        this.registrationNumber = registrationNumber;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSex() {
//        return sex;
//    }
//
//    public void setSex(String sex) {
//        this.sex = sex;
//    }
//
//    public Integer getAge() {
//        return age;
//    }
//
//    public void setAge(Integer age) {
//        this.age = age;
//    }
//
//    public String getMobile() {
//        return mobile;
//    }
//
//    public void setMobile(String mobile) {
//        this.mobile = mobile;
//    }
//
//    public String getDepartmentName() {
//        return departmentName;
//    }
//
//    public void setDepartmentName(String departmentName) {
//        this.departmentName = departmentName;
//    }
//
//    public String getTruename() {
//        return truename;
//    }
//
//    public void setTruename(String truename) {
//        this.truename = truename;
//    }
//
//    public String getUpdateTime() {
//        return updateTime;
//    }
//
//    public void setUpdateTime(String updateTime) {
//        this.updateTime = updateTime;
//    }
//
//    public Integer getRegistrationStatus() {
//        return registrationStatus;
//    }
//
//    public void setRegistrationStatus(Integer registrationStatus) {
//        this.registrationStatus = registrationStatus;
//    }
//
//    @Override
//    public String toString() {
//            return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
//                    .append("registrationId", getRegistrationId())
//                    .append("registrationNumber", getRegistrationNumber())
//                    .append("name", getName())
//                    .append("sex", getSex())
//                    .append("age", getAge())
//                    .append("mobile", getMobile())
//                    .append("departmentName", getDepartmentName())
//                    .append("registrationStatus", getRegistrationStatus())
//                    .append("truename", getTruename())
//                    .append("updateTime", getUpdateTime())
//                    .append("", )
//                    .append("", )
//                    .toString();
//    }
}
