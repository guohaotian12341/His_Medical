package com.ruoyi.medicallist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.medicallist.domain.PersonalDetails;

public interface IPersonalDetailsService extends IService<PersonalDetails> {

    PersonalDetails PersonBackNo(Integer registrationId);

    PersonalDetails OrderBackno(Integer registrationId);

    PersonalDetails RegistrationBackno(Integer registrationId);
}
