package com.ruoyi.medicallist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.medicallist.domain.PersonalDetails;
import com.ruoyi.medicallist.mapper.MedicallistMapper;
import com.ruoyi.medicallist.mapper.PersonalDetailsMapper;
import com.ruoyi.medicallist.service.IPersonalDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class PersonalDetailsImpl extends ServiceImpl<PersonalDetailsMapper, PersonalDetails> implements IPersonalDetailsService{

    @Autowired
    private PersonalDetailsMapper mapper;

    @Override
    public PersonalDetails PersonBackNo(Integer registrationId) {
        return mapper.PersonBackNo(registrationId);
    }

    @Override
    public PersonalDetails OrderBackno(Integer registrationId) {
        return mapper.OrderBackno(registrationId);
    }

    @Override
    public PersonalDetails RegistrationBackno(Integer registrationId) {
        return mapper.RegistrationBackno(registrationId);
    }
}
