package com.ruoyi.medicallist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import com.ruoyi.medicallist.domain.HisMedicallist;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
@DataSource(value = DataSourceType.SLAVE)
public interface MedicallistMapper extends BaseMapper<HisMedicallist> {

    List<HisMedicallist> selectMedicalList(HisMedicallist hm);

    HisMedicallist BackNo(Integer registrationId);

    int BackNoUpdate(Integer registrationId);
}
