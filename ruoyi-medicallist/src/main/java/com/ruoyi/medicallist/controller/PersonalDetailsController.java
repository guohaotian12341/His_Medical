package com.ruoyi.medicallist.controller;

import com.ruoyi.medicallist.service.IPersonalDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/his/personalDetails")
public class PersonalDetailsController {
    @Autowired
    private IPersonalDetailsService service;

    //患者信息
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/personbackno/{registrationId}")
    public Object PersonBackNo(@PathVariable Integer registrationId){
        return service.PersonBackNo(registrationId);
    }

    //订单信息
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/orderbackno/{registrationId}")
    public Object OrderBackno(@PathVariable Integer registrationId){
        return service.OrderBackno(registrationId);
    }

    //挂号信息
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/registrationbackno/{registrationId}")
    public Object RegistrationBackno(@PathVariable Integer registrationId){
        return service.RegistrationBackno(registrationId);
    }
}
