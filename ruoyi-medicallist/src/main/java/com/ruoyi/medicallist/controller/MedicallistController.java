package com.ruoyi.medicallist.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.medicallist.domain.HisMedicallist;
import com.ruoyi.medicallist.service.IMedicallistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/his/medicallist")
public class MedicallistController extends BaseController {
    @Autowired
    private IMedicallistService service;

    /**
     * 查询门诊挂号列表
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/list")
    public TableDataInfo list(HisMedicallist hm)
    {
        startPage();
        List<HisMedicallist> list = service.selectMedicalList(hm);
        return getDataTable(list);
    }

    /**
     *查询退号信息详细
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/backno/{registrationId}")
    public Object BackNo(@PathVariable Integer registrationId){
        return service.BackNo(registrationId);
    }

    /**
     * 退号修改挂号状态
     */
    @PreAuthorize("@ss.hasPermi('chief') or @ss.hasPermi('department')")
    @GetMapping("/update/{registrationId}")
    public Object BackNoUpdate(@PathVariable Integer registrationId){
        return service.BackNoUpdate(registrationId);
    }
}
